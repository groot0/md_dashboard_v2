
<div class="card-block">
	
		{{ Form::open(['class' => '', 'files' => true, 'url' => 'department', 'method' => 'POST', 'autocomplete' => 'off']) }}

			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
					  	<label for="department_name">Department Name:</label>
					  	<input type="text" id="department_name" required class="form-control {{ $errors->has('department_name') ? ' is-invalid' : '' }}" value="{{ old('department_name') }}" name="department_name"/>
					  	@if ($errors->has('department_name'))
						    <span class="invalid-feedback" role="alert" >
								<span class="messages"><strong>{{ $errors->first('department_name') }}</strong></span>
							</span>
						@endif
					</div>
				</div>
			</div>
			<div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary waves-effect waves-light ">Save</button>
            </div>

		{{ Form::close()}}
	
</div>
           