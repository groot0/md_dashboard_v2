
@if( isset( $focuses ) )
	@foreach($focuses as $focus)
		
        <div class="card-block">
			
				{{ Form::open(['class' => '', 'files' => true, 'url' => 'focus/'.$focus->id, 'method' => 'PUT', 'autocomplete' => 'off']) }}
		
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
							  	<label for="focus_name">Focus Name:</label>
							  	<input type="text" required id="focus_name" class="form-control {{ $errors->has('focus_name') ? ' is-invalid' : '' }}" value="{{ $focus->focus_name }}" name="focus_name"/>
							  	@if ($errors->has('focus_name'))
								    <span class="invalid-feedback" role="alert" >
										<span class="messages"><strong>{{ $errors->first('focus_name') }}</strong></span>
									</span>
								@endif
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label class="description" for="description">Description: </label>
								<textarea rows="3" cols="3" class="form-control" id="description" name="description">{{ $focus->description }}</textarea>
								@if ($errors->has('description'))
								    <span class="invalid-feedback" role="alert" >
										<span class="messages"><strong>{{ $errors->first('description') }}</strong></span>
									</span>
								@endif
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label for="focus_end_date">End Date :</label>
							  	<input type="" class="form-control datepicker  {{ $errors->has('focus_end_date') ? ' is-invalid' : '' }}" 
							  	@if(isset($focus->focus_end_date)) 
							  		value="{{ date('d-m-Y', strtotime($focus->focus_end_date)) }}"
							  	@endif name="focus_end_date"/>
							  	@if ($errors->has('focus_end_date'))
								    <span class="invalid-feedback" role="alert" >
										<span class="messages"><strong>{{ $errors->first('focus_end_date') }}</strong></span>
									</span>
								@endif
							</div>
						</div>
					</div>
					<div class="modal-footer">
		                <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
		                <button type="submit" class="btn btn-primary waves-effect waves-light ">Update</button>
		            </div>

				{{ Form::close()}}
			<script type="text/javascript" src="{{ asset('public/js/custom.js') }}"></script>
		</div>
		            
	@endforeach
@endif
