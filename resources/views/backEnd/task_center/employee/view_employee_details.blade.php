@if (isset($employee))
<style type="text/css">
  .input_time{
    margin-top: 8px;
  }
</style>
<div class="card user-card">
  <div class="card-block text-center">
    <div class="usre-image">
      @if(empty($employee->employee_photo))
      <img src="{{asset('public/images/no_image.png')}}" alt="user image" class="img-radius img-40 align-top m-r-15">
      @else
      <img src="{{asset($employee->employee_photo)}}" alt="user image" class="img-radius img-40 align-top m-r-15">
      @endif
    </div>
    <h6 class="m-t-25 m-b-10">@if(isset($employee->full_name)){{$employee->full_name}} @endif
    </h6>
    <p class="text-muted">Designation - @if(isset($employee->designation_id)){{$employee->designation->designation_name}} @endif
    </p>
    <p class="text-muted">Department - @if(isset($employee->department_id)){{$employee->department->department_name}} @endif
    </p>
    <p class="text-muted">Mobile - @if(isset($employee->full_name)){{$employee->mobile}} @endif
    </p>
    <p class="text-muted">Email - @if(isset($employee->full_name)){{$employee->email}} @endif
    </p>
    <a href="#!" class="text-c-red d-block">
    </a>
    
    <div class="row">
      <div class="col-md-12">
      <form id="employee_attend_time_wrapper" method="GET">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <input type="hidden" name="url" id="url" value="{{URL::to('/')}}"> 
    <input type="hidden" name="emp_id" id="emp_id" value="@if(isset($employee->id)){{$employee->id}} @endif">
        <table class="table table-striped table-bordered nowrap basic-btn">
          <thead>
            <tr>
              <th>
                <span class="">In Time
                </span>
              </th>
              <th>
                <span class="">Late Time
                </span>
              </th>
              <th>
                <span class="">
                </span>
              </th>
            </tr>
          </thead>
          <input type="hidden" name="url" id="url" value="{{URL::to('/')}}">
          <tbody>
            <tr>
              
              <td> 
                <input class="form-control input_time" type="time" id="late_time" required="required" 
                value="@if(isset($all_employee_late_time_details)){{ $employee->checkLateTime( $all_employee_late_time_details->late_time ) }}@endif">
              </td>

              <td>
                <input class="form-control input_time" type="time" id="too_late" required="required" value="@if(isset($all_employee_late_time_details)){{ $employee->checkAbsentTime( $all_employee_late_time_details->absent ) }}@endif">
              </td>
              <td>
                <button class="btn btn-success btn-sm" type="submit">Update</button>
              </td>
            </tr>
          </tbody>
        </table>
      </form>
    </div>
  </div>
  </div>
</div>

@endif
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
<script>
$(document).ready(function(){
  $("#employee_attend_time_wrapper").submit(function(e){
    e.preventDefault();
    var url = $('#url').val();
    var emp_id = $("#emp_id").val();
    var late_time = $("#late_time").val();
    var too_late = $("#too_late").val();
   
    $.ajax({
      type: "POST",
      url: url + '/emp_late_time',
      data: { late_time: late_time, too_late:too_late, emp_id:emp_id,  _token: '{{csrf_token()}}' },
      datatype: "html",
        success: function(data){
        
      console.log(data);
          e.preventDefault();
          toastr.success('Updated Successfully', 'Success', {timeOut: 5000})
        }        
      });

   });
  });
</script>
