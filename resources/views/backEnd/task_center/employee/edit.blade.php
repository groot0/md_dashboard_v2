@if ($mds_employees)
	@foreach($mds_employees as $employee)
	    <div class="card-block">
			{{ Form::open(['class' => '', 'files' => true, 'url' => 'employee/'.$employee->id, 'method' => 'PUT', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off']) }}
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
						  	<label for="first_name">Employee Name:</label>
						  	<input type="text" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" value="{{$employee->first_name}}" name="first_name"/>
						  	@if ($errors->has('first_name'))
							    <span class="invalid-feedback" role="alert" >
									<span class="messages"><strong>{{ $errors->first('first_name') }}</strong></span>
								</span>
							@endif
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
						  	<label for="email">Email:</label>
						  	<input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{$employee->email}}" required="required" name="email"/>
						  	@if ($errors->has('email'))
							    <span class="invalid-feedback" role="alert" >
									<span class="messages"><strong>{{ $errors->first('email') }}</strong></span>
								</span>
							@endif
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
						  	<label for="designation_id">Designation name:</label>
						  	<select class="form-control {{ $errors->has('designation_id') ? ' is-invalid' : '' }}" name="designation_id" id="designation_id">
							<option value="">Select Designation</option>
							@if(isset($designations))
								@foreach($designations as $key=>$value)
									<option value="{{$value->id}}"
										@if(isset($employee))
											@if($employee->designation_id == $value->id)
												selected
											@endif
										@endif
										>{{$value->designation_name}}
									</option>
								@endforeach
							@endif
							</select>
							@if ($errors->has('designation_id'))
							<span class="invalid-feedback invalid-select" role="alert">
								<strong>{{ $errors->first('designation_id') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
						  <label for="mobile">Phone no:</label>
						  <input type="text" class="form-control {{ $errors->has('mobile') ? ' is-invalid' : '' }}" value="{{$employee->mobile}}" name="mobile"/>
						  	@if ($errors->has('mobile'))
							    <span class="invalid-feedback" role="alert" >
									<span class="messages"><strong>{{ $errors->first('mobile') }}</strong></span>
								</span>
							@endif
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
						  	<label for="employee_photo">Employee Image:</label><br>
						  	<input data-preview="#preview" name="employee_photo" type="file" id="employee_photo">
						  	@if ($errors->has('employee_photo'))
							    <span class="invalid-feedback" role="alert" >
									<span class="messages"><strong>{{ $errors->first('employee_photo') }}</strong></span>
								</span>
							@endif
						</div>
					</div>
					<input type="hidden" class="form-control" value="mds_desk" name="employee_type_id"/>
				</div>
				<div class="modal-footer">
	                <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
	                <button type="submit" class="btn btn-primary waves-effect waves-light ">Update employee</button>
	            </div>
			{{ Form::close()}}
		</div>
	@endforeach
@endif
