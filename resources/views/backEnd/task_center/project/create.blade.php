
<div class="card-block">
	{{ Form::open(['class' => '', 'files' => true, 'url' => 'project', 'method' => 'POST', 'autocomplete' => 'off', 'enctype' => 'multipart/form-data']) }}
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
				  	<label for="project_name">Project name:</label>
				  	<input type="text" required="required" class="form-control  {{ $errors->has('project_name') ? ' is-invalid' : '' }}" value="{{ old('project_name') }}" name="project_name" />
					@if ($errors->has('project_name'))
					<span class="invalid-feedback invalid-select" role="alert">
						<strong>{{ $errors->first('project_name') }}</strong>
					</span>
					@endif
				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group">
					<label for="project_start_date">Project Start date:</label>
				  	<input type="" class="form-control datepicker  {{ $errors->has('project_start_date') ? ' is-invalid' : '' }}" value="{{ old('project_start_date') }}" name="project_start_date"/>
				  	@if ($errors->has('project_start_date'))
					    <span class="invalid-feedback" role="alert" >
							<span class="messages"><strong>{{ $errors->first('project_start_date') }}</strong></span>
						</span>
					@endif
				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group">
					<label for="project_end_date">Project End date:</label>
				  	<input type="" class="form-control datepicker  {{ $errors->has('project_end_date') ? ' is-invalid' : '' }}" value="{{ old('project_end_date') }}" name="project_end_date"/>
				  	@if ($errors->has('project_end_date'))
					    <span class="invalid-feedback" role="alert" >
							<span class="messages"><strong>{{ $errors->first('project_end_date') }}</strong></span>
						</span>
					@endif
				</div>
			</div>

		</div>
		<div class="modal-footer">
            <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary waves-effect waves-light ">Save changes</button>
        </div>
	{{ Form::close()}}
	<script type="text/javascript" src="{{ asset('public/js/custom.js') }}"></script>
</div>
            