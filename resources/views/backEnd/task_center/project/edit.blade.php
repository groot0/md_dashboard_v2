@if( isset( $projects ) )
	@foreach($projects as $project)

        <div class="card-block">
			{{ Form::open(['class' => '', 'files' => true, 'url' => 'project/'.$project->id, 'method' => 'PUT', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off']) }}
				<div class="row">

					<div class="col-md-12">
						<div class="form-group">
						  	<label for="project_name">Project name:</label>
						  	<input type="text" required="required" class="form-control {{ $errors->has('project_name') ? ' is-invalid' : '' }}" name="project_name" value="{{$project->project_name }}" />
							@if ($errors->has('project_name'))
							<span class="invalid-feedback invalid-select" role="alert">
								<strong>{{ $errors->first('project_name') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label for="project_start_date">Project Start Date:</label>
						  	<input type="" class="form-control datepicker  {{ $errors->has('project_start_date') ? ' is-invalid' : '' }}" 
						  	@if(isset($project->project_start_date)) 
						  		value="{{ date('d-m-Y', strtotime($project->project_start_date)) }}"
						  	@endif name="project_start_date"/>
						  	@if ($errors->has('project_start_date'))
							    <span class="invalid-feedback" role="alert" >
									<span class="messages"><strong>{{ $errors->first('project_start_date') }}</strong></span>
								</span>
							@endif
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label for="project_end_date">Project End Date:</label>
						  	<input type="" class="form-control datepicker  {{ $errors->has('project_end_date') ? ' is-invalid' : '' }}" 
						  	@if(isset($project->project_end_date)) 
						  		value="{{ date('d-m-Y', strtotime($project->project_end_date)) }}"
						  	@endif name="project_end_date"/>
						  	@if ($errors->has('project_end_date'))
							    <span class="invalid-feedback" role="alert" >
									<span class="messages"><strong>{{ $errors->first('project_end_date') }}</strong></span>
								</span>
							@endif
						</div>
					</div>
					
				</div>
				<div class="modal-footer">
	                <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
	                <button type="submit" class="btn btn-primary waves-effect waves-light ">Update</button>
	            </div>
			{{ Form::close()}}
			<script type="text/javascript" src="{{ asset('public/js/custom.js') }}"></script>
		</div>
		            
	@endforeach
@endif