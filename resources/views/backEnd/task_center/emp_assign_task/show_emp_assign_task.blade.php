   
@if (isset($assign_projects) && count($assign_projects) > 0)
	<div class="card-block">
	    <div class="view-info">
	        <div class="row">
	            <div class="col-lg-12">
	                <div class="general-info">
	                    <div class="row">
	                        
	                        <div class="col-lg-12">
	                            <div class="table-responsive">
	                                <table class="table m-0">
	                                	<thead>
                                            <tr>
                                                <th>Project name</th>
                                                <th>Task name</th>
                                                <th>Task start date</th>
                                                <th>Task end date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        	@foreach ($assign_projects as $assign_project)
                                        		<tr>
                                        			{{-- Project name --}}
	                                        		@if (count( $projects ) > 0 )
		                                            	@php
		                                            		$project_flag = 0;
		                                            	@endphp
		                                            	@foreach($projects as $project)
															@if($assign_project->project_id == $project->id)
																<td>{{ $project->project_name }}</td>
																@php
																	$project_flag = 1;
																@endphp
															@endif
														@endforeach
														@if ($project_flag == 0)
															<td>There is no project.</td>
														@endif
		                                            @else
		                                            	<td>There is no project.</td>
		                                            @endif

		                                            {{-- task name --}}
		                                            @if (isset($assign_project->task_name))
		                                            	<td>{{$assign_project->task_name}}</td>
		                                            @else
		                                            	<td>No task given</td>
		                                            @endif

		                                            {{-- task start date --}}
		                                            @if (isset($assign_project->task_start_date))
		                                            	<td>{{ date('d-m-Y', strtotime($assign_project->task_start_date) )}}</td>
		                                            @else
		                                            	<td>No date given</td>
		                                            @endif

		                                            {{-- task end date --}}
		                                            @if (isset($assign_project->task_end_date))
		                                            	<td>{{ date('d-m-Y', strtotime($assign_project->task_end_date) )}}</td>
		                                            @else
		                                            	<td>No date given</td>
		                                            @endif
		                                        </tr>
                                      
                                        	@endforeach
                                        </tbody>
	                                </table>
	                            </div>
	                        </div>
	                        
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="modal-footer">
	        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
	    </div>
	</div>
@else
	<div class="card-block">
	    <div class="view-info">
	        <div class="row">
	            <div class="col-lg-12">
	                <div class="general-info">
	                    <div class="row">
	                        
	                        <div class="col-lg-12">
	                            <div class="table-responsive">
	                                <table class="table m-0">
	                                    <tbody>
	                                        <tr>
	                                            <th scope="row">There are no employee task assigned.</th>
	                                            <td></td>
	                                        </tr>
	                                    </tbody>
	                                </table>
	                            </div>
	                        </div>
	                        
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="modal-footer">
	        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
	    </div>
	</div>
@endif

