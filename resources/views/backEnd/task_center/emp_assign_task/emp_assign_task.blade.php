
<div class="card-block">
	{{ Form::open(['class' => '', 'files' => true, 'url' => 'emp_task_assign', 'method' => 'POST', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off']) }}
		<div class="row">

			<div class="col-md-12">
				<div class="form-group">
				  	<label for="employee_id">Employee name:</label>
				  	<input type="hidden" name="employee_id" value="{{$id}}">
				  	<select class="form-control {{ $errors->has('employee_id') ? ' is-invalid' : '' }}" disabled="disabled" required="required" >
					<option value="">Select Employee</option>
						@foreach($employees as $key=>$value)
							<option value="{{$value->id}}"
							
									@if($id == $value->id)
										selected
									@endif
						
								>{{$value->full_name}}
							</option>
						@endforeach

					</select>
					@if ($errors->has('employee_id'))
					<span class="invalid-feedback invalid-select" role="alert">
						<strong>{{ $errors->first('employee_id') }}</strong>
					</span>
					@endif
				</div>
			</div>

			<div class="col-md-12">
			    <div class="form-group">
			        <label for="project_id">Project name:</label>
			        <select class="form-control {{ $errors->has('project_id') ? ' is-invalid' : '' }}" required="required" name="project_id" id="project_id">
			          	<option value="">Select Project</option>
				            @if (count( $projects ) > 0)
					            @foreach($projects as $key=>$value)
					            	<option value="{{$value->id}}">{{$value->project_name}}	</option>
					            @endforeach
				            @endif
			        </select>
			        @if ($errors->has('project_id'))
				        <span class="invalid-feedback invalid-select" role="alert">
				            <strong>{{ $errors->first('project_id') }}</strong>
				        </span>
			        @endif
			    </div>
			</div>

			<div class="col-md-12">
			    <div class="form-group">
			        <label for="task_name">Task Name:</label>
			        <input type="text" class="form-control {{ $errors->has('task_name') ? ' is-invalid' : '' }}" value="{{old('task_name')}}" name="task_name" />
			        @if ($errors->has('task_name'))
				        <span class="invalid-feedback" role="alert">
				            <span class="messages"><strong>{{ $errors->first('task_name') }}</strong></span>
				        </span>
			        @endif
			    </div>
			</div>

			<div class="col-md-12">
			    <div class="form-group">
			        <label for="task_start_date">Task Start Date:</label>
			        <input type="" name="task_start_date" class="form-control datepicker  {{ $errors->has('task_start_date') ? ' is-invalid' : '' }}" value="{{ old('task_start_date') }}"/>
			        @if ($errors->has('task_start_date'))
				        <span class="invalid-feedback" role="alert">
				            <span class="messages"><strong>{{ $errors->first('task_start_date') }}</strong></span>
				        </span>
			        @endif
			    </div>
			</div>

			<div class="col-md-12">
			    <div class="form-group">
			        <label for="task_end_date">Task End Date:</label>
			        <input type="" name="task_end_date" class="form-control datepicker" value="{{ old('task_end_date') }}"/>
			        @if ($errors->has('task_end_date'))
				        <span class="invalid-feedback" role="alert">
				            <span class="messages"><strong>{{ $errors->first('task_end_date') }}</strong></span>
				        </span>
			        @endif
			    </div>
			</div>

		</div>
		<div class="modal-footer">
            <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary waves-effect waves-light ">Save task</button>
        </div>
	{{ Form::close()}}
	<script type="text/javascript" src="{{ asset('public/js/custom.js') }}"></script>
</div>         
