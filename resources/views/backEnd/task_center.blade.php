@extends('backEnd.master')
@section('mainContent')
<style type="text/css">
    .employee_present{
        margin-bottom: 20px;
        margin-left: -18px;
    }
</style>
    @if(session()->has('message-success'))
        <div class="alert alert-success mb-3 background-success" role="alert">
            {{ session()->get('message-success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @elseif(session()->has('message-danger'))
        <div class="alert alert-danger">
            {{ session()->get('message-danger') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    @if(session()->has('message-success-delete'))
        <div class="alert alert-danger mb-3 background-danger" role="alert">
            {{ session()->get('message-success-delete') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @elseif(session()->has('message-danger-delete'))
        <div class="alert alert-danger">
            {{ session()->get('message-danger-delete') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <script type="text/javascript" src="{{ asset('public/assets/mega_able/js/jquery.min.js') }}"></script>
    <input type="hidden" name="url" id="url" value="{{URL::to('/')}}">


    <div class="row m-t-40">
        <div class="col-lg-8">
            <div class="card">
              <div class="card-header department_card_header">
                      <h5 class="make_font_white">Attendence Report</h5>

                      <a class="btn btn-success btn-search-attendance" href="{{url('employee-attendence')}}">Search attendance</a>
              </div>
                <div class="card-block">

                    <div class="row">
                        <div class="col-md-12 employee_present">
                                <span class="text-muted m-b-10 col-md-4"><strong>Total Employees:</strong>{{$employee_attendance_statistics['total']}}</span>
                                <span class="text-muted m-b-10 col-md-4"><strong>Present:</strong>{{$employee_attendance_statistics['present']}}</span>
                                <span class="text-muted m-b-10 col-md-4"><strong>Absent:</strong>{{$employee_attendance_statistics['absent']}}</span>

                    </div>
                    </div>

                    <div class="dt-responsive table-responsive">
                        <table class="table table-striped table-bordered nowrap basic-btn" id="employee_attendance_datatable">
                            <div class="row">
                                <div class="col-md-4">
                                     Department:
                                <select id="select_department" class="form-control">
                                    <option value="">All</option>
                                    @if (isset($departments))
                                        @foreach ($departments as $department)
                                            <option>{{$department->department_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                </div>
                               <div class="col-md-4">

                               </div>

                                <div class="col-md-4">
                                    <i class="fa fa-calendar date_pick" aria-hidden="true"></i>
                                    <div id = "divDatePicker"></div>

                                    <button id="btn_yesteday" class="btn-default btn-sm yesterday_btn gray_clr">Yesterday</button>
                                    <button id="btn_today" class="btn-default btn-sm yesterday_btn gray_clr">Today</button>
                                    <input type="hidden" name="url" id="url" value="{{URL::to('/')}}">
                                </div>
                            </div>

                            <thead>
                                <tr>
                                    <th>Employee</th>
                                    <th>Deparment</th>
                                    <th>In Time</th>
                                    <th>Out Time</th>
                                    <th>Daily Hours</th>
                                    <th>Monthly Hours</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($sortedEmployees as $employee)

                                    @php
                                        $empColor = $employee->rowColor( $office_times->late_time, $office_times->absent );
                                    @endphp

                                    <tr id="{{ $empColor }}">

                                    <td>
                                        <a class="modalLink" @if ($empColor == 'red_row') id="font_color_white" @endif title="View employee" data-modal-size="modal-md" href="{{ url('view_employee', $employee->id) }}" >{{ $employee->full_name }}</a>

                                        {!! $employee->badgeColor($empColor) !!}
                                    </td>

                                    <td>{{$employee->department['department_name']}}</td>

                                    <td>{{$employee->getInTime()}}</td>

                                    <td>{{$employee->getOutTime()}}</td>

                                    <td>{{$employee->today_total_hours()}}</td>

                                    <td>{{ $employee->monthly_total_hour() }}</td>

                                  </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="row">

                <!-- <div class="col-lg-6 sector-section">
                    <div class="card ">
                        <div class="card-header focus_project_header">
                            <h5 class="make_font_white">Absentee List (Today - <?php echo date('d-M-Y'); ?>)</h5>
                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table  class="table table-striped table-bordered nowrap basic-btn">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                            @if (isset($emp_attendes) && isset($all_employees))
                                                @php
                                                    $count_absent = 0;
                                                @endphp
                                                @foreach ($all_employees as $employee)
                                                    @php
                                                        $flag = 0;
                                                    @endphp
                                                    @foreach ($emp_attendes as $result)
                                                        @if ($employee->device_USERID == $result['emp_id'])
                                                            @php
                                                                $flag = 1;
                                                            @endphp
                                                        @endif
                                                    @endforeach

                                                    @if ($flag == 0)
                                                        @php $count_absent += 1; @endphp
                                                        <tr> <td><a style="font-weight: 500;" class="modalLink" title="Employee Details" data-modal-size="modal-md" href="{{ url('viewEmployeeById', $employee->id) }}" >@php echo $employee->full_name @endphp</a></td> </tr>
                                                    @endif

                                                @endforeach
                                                @if ($count_absent == 0)
                                                    <tr><td>No employees are absent.</td></tr>
                                                @endif
                                            @endif

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div> -->

                <div class="col-lg-12">
                    <div class="card ">
                        <div class="card-header md_desk_card_header">
                            <h5 class="make_font_white">MD's Desk</h5>
                            <div class="card-header-right">
                                <a class="modalLink waves-effect" title="Create MD employee" data-modal-size="modal-md" href="{{ url('create_md_employee_modal') }}"><i class="fa fa fa-plus make_font_white"></i></a>
                            </div>
                        </div>
                        <div class="card-block">
                            @if( isset($employees) && count($employees) > 0)
                                @foreach($employees as $employee)
                                    <div class="align-middle m-b-30">
                                        @if( isset($employee->employee_photo)  )
                                            <img src="{{ asset($employee->employee_photo) }}" alt="user image" class="img-radius img-40 align-top m-r-15">
                                        @else
                                            <img src="{{ asset('public/images/no_image.png') }}" alt="user image" class="img-radius img-40 align-top m-r-15">
                                        @endif

                                        <div class="d-inline-block">
                                            @if( isset($employee->full_name) && (isset($employee->first_name) || isset($employee->last_name)) )
                                                <h6>{{ $employee->full_name }} <span class="badge bg-c-green"></span></h6>
                                            @else
                                                <h6>No name given <span class="badge bg-c-green badge_red"></span> </h6>
                                            @endif

                                            @if(isset($designations))
                                                @php $flag = 0 @endphp
                                                @foreach($designations as $designation)
                                                    @if($designation->id == $employee->designation_id)
                                                        @php $flag = 1 @endphp
                                                        <p class="text-muted m-b-0">{{ $designation->designation_name }}</p>
                                                    @endif
                                                @endforeach
                                                @if($flag == 0)
                                                    <p class="text-muted m-b-0">No designation given</p>
                                                @endif
                                            @else
                                                <p class="text-muted m-b-0">No designation given</p>
                                            @endif
                                        </div>
                                        <div class="float_right">

                                            <a class="modalLink view_icon" title="View employee task" data-modal-size="modal-md" href="{{ url('view_assign_task', $employee->id) }}" ><i class="fas fa-eye"></i></a>
                                            <a class="modalLink assign_icon" title="Assign project/task" data-modal-size="modal-md" href="{{ url('assign_task', $employee->id) }}" ><i class="fas fa-tasks"></i></a>
                                            <a class="modalLink edit_icon" title="Edit sector" data-modal-size="modal-md" href="{{ url('edit_md_employee_modal', $employee->id) }}" ><i class="fas fa-edit"></i></a>
                                            <a class="modalLink delete_icon" title="Delete"  data-modal-size="modal-md" href="{{url('deleteEmployeeView', $employee->id)}}" ><i class="fa fa-trash"></i></a>
                                        </div >
                                    </div>
                                @endforeach
                            @else
                                <p>There is no employees</p>
                            @endif

                            <div class="text-center">
                                <a href="{{ url('employee') }}" class="b-b-primary text-primary">View all Employee</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            {{-- <div class="row">
                <div class="col-lg-6 sector-section">
                    <div class="card ">
                        <div class="card-header">
                            <h5 class="make_font_white">Sector</h5>
                            <div class="card-header-right">
                                <a class="modalLink waves-effect" title="Create Sector" data-modal-size="modal-md" href="{{ url('create_sector_modal') }}"><i class="fa fa fa-plus make_font_white"></i></a>
                            </div>
                        </div>
                        <div class="card-block">
                            <ol class="sector-list">
                                @foreach($sectors as $sector)
                                <li>
                                    <div class="align-middle m-b-30">
                                        <div class="d-inline-block">
                                            <h6>{{ $sector->name }}</h6>
                                        </div>
                                        <div class="float_right">
                                            <a class="modalLink edit_icon" title="Edit sector" data-modal-size="modal-md" href="{{ url('edit_sector_modal', $sector->id) }}" ><i class="fas fa-edit"></i></a>
                                            <a class="modalLink delete_icon" title="Delete"  data-modal-size="modal-md" href="{{url('deleteSectorView', $sector->id)}}" ><i class="fa fa-trash"></i></a>
                                        </div >
                                    </div>
                                </li>
                                @endforeach
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card ">
                        <div class="card-header operation_card_header">
                            <h5 class="make_font_white">Operations</h5>
                            <div class="card-header-right">
                                <a class="modalLink waves-effect" title="Create Operation employee" data-modal-size="modal-md" href="{{ url('create_operation_modal') }}"><i class="fa fa fa-plus make_font_white"></i></a>
                            </div>
                        </div>
                        <div class="card-block">
                            @if( isset($operational_employees) && count($operational_employees) > 0)
                                @foreach($operational_employees as $employee)
                                    <div class="align-middle m-b-30">
                                        @if( isset($employee->employee_photo)  )
                                            <img src="{{ asset($employee->employee_photo) }}" alt="user image" class="img-radius img-40 align-top m-r-15">
                                        @else
                                            <img src="{{ asset('public/images/no_image.png') }}" alt="user image" class="img-radius img-40 align-top m-r-15">
                                        @endif

                                        <div class="d-inline-block">
                                            @if( isset($employee->full_name) && (isset($employee->first_name) || isset($employee->last_name)) )
                                                <h6>{{ $employee->full_name }}<span class="badge bg-c-green"></span></h6>
                                            @else
                                                <h6>No name given<span class="badge bg-c-green badge_red"></span></h6>
                                            @endif

                                            @if(isset($designations))
                                                @php $flag = 0 @endphp
                                                @foreach($designations as $designation)
                                                    @if($designation->id == $employee->designation_id)
                                                        @php $flag = 1 @endphp
                                                        <p class="text-muted m-b-0">{{ $designation->designation_name }}</p>
                                                    @endif
                                                @endforeach
                                                @if($flag == 0)
                                                    <p class="text-muted m-b-0">No designation given</p>
                                                @endif
                                            @else
                                                <p class="text-muted m-b-0">No designation given</p>
                                            @endif
                                        </div>
                                        <div class="float_right">
                                            <a class="modalLink edit_icon" title="Edit operation employee" data-modal-size="modal-md" href="{{ url('edit_operation_modal', $employee->id) }}" ><i class="fas fa-edit"></i></a>
                                            <a class="modalLink delete_icon" title="Delete"  data-modal-size="modal-md" href="{{url('deleteEmployeeView', $employee->id)}}" ><i class="fa fa-trash"></i></a>
                                        </div >
                                    </div>
                                @endforeach
                            @else
                                <p>There is no employees</p>
                            @endif
                            <div class="text-center">
                                <a href="{{ url('employee') }}" class="b-b-primary text-primary">View all Operations</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="card ">
                        <div class="card-header department_card_header">
                            <h5 class="make_font_white">Departments</h5>
                            <div class="card-header-right">
                                <a class="modalLink waves-effect" title="Create Department" data-modal-size="modal-md" href="{{ url('create_department_modal') }}"><i class="fa fa fa-plus make_font_white"></i></a>
                            </div>
                        </div>
                        <div class="card-block">
                            @if( isset($departments) && count($departments) > 0)
                                @php $i =1; @endphp
                                @foreach($departments as $department)
                                    <div class="align-middle m-b-30">
                                        <div class="d-inline-block">
                                            <h6>{{$i++}}. {{ $department->department_name }}</h6>
                                        </div>
                                        <div class="float_right">
                                            <a class="modalLink edit_icon" title="Edit Department" data-modal-size="modal-md" href="{{ url('edit_department_modal', $department->id) }}" ><i class="fas fa-edit"></i></a>
                                            <a class="modalLink delete_icon" title="Delete"  data-modal-size="modal-md" href="{{url('deleteDepartmentView', $department->id)}}" ><i class="fa fa-trash"></i></a>
                                        </div >
                                    </div>
                                @endforeach
                            @else
                                <p>There is no Departments.</p>
                            @endif
                            <div class="text-center">
                                <a href="{{ url('department') }}" class="b-b-primary text-primary">View all Departments</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card ">
                        <div class="card-header focus_project_header">
                            <h5 class="make_font_white">Projects</h5>
                            <div class="card-header-right">
                                <a class="modalLink waves-effect" title="Create Project" data-modal-size="modal-md" href="{{ url('create_project_modal') }}"><i class="fa fa fa-plus make_font_white"></i></a>
                            </div>
                        </div>
                        <div class="card-block">
                            @if( isset($projects) && count($projects) > 0)
                                @php $i =1; @endphp
                                @foreach($projects as $project)

                                    <div class="align-middle m-b-30">
                                        <div class="d-inline-block">
                                            <h6>{{$i++}}. {{ $project->project_name }}</h6>
                                        </div>
                                        <div class="float_right">
                                            <a class="modalLink edit_icon" title="Edit Project" data-modal-size="modal-md" href="{{ url('edit_project_modal', $project->id) }}" ><i class="fas fa-edit"></i></a>
                                            <a class="modalLink delete_icon" title="Delete"  data-modal-size="modal-md" href="{{url('deleteProjectView', $project->id)}}" ><i class="fa fa-trash"></i></a>
                                        </div >
                                    </div>

                                @endforeach
                            @else
                                <p>There is no Project.</p>
                            @endif
                            <div class="text-center">
                                <a href="{{ url('project') }}" class="b-b-primary text-primary">View all projects.</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="card ">
                        <div class="card-header focus_card_header">
                            <h5 class="make_font_white">Focus</h5>
                            <div class="card-header-right">
                                <a class="modalLink waves-effect" title="Create Focus" data-modal-size="modal-md" href="{{ url('create_focus_modal') }}"><i class="fa fa fa-plus make_font_white"></i></a>
                            </div>
                        </div>
                        <div class="card-block">
                            @if( isset($focuses) && count($focuses) > 0)
                                @php $i =1; @endphp
                                @foreach($focuses as $focus)

                                    <div class="align-middle m-b-30">
                                        <div class="d-inline-block">
                                            <h6>{{$i++}}. {{ $focus->focus_name }}</h6>
                                        </div>
                                        <div class="float_right">
                                            <a class="modalLink edit_icon" title="Edit Focus" data-modal-size="modal-md" href="{{ url('edit_focus_modal', $focus->id) }}" ><i class="fas fa-edit"></i></a>
                                            <a class="modalLink delete_icon" title="Delete"  data-modal-size="modal-md" href="{{url('deleteFocusView', $focus->id)}}" ><i class="fa fa-trash"></i></a>
                                        </div >
                                    </div>

                                @endforeach
                            @else
                                <p>There is no Focus.</p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card ">
                        <div class="card-header company_card_header">
                            <h5 class="make_font_white">Company</h5>
                            <div class="card-header-right">
                                <a class="modalLink waves-effect" title="Create Company" data-modal-size="modal-md" href="{{ url('create_company_modal') }}"><i class="fa fa fa-plus make_font_white"></i></a>
                            </div>
                        </div>
                        <div class="card-block">
                            @php $i = 1; @endphp
                            @foreach($companies as $company)
                                <div class="align-middle m-b-30">
                                    <div class="d-inline-block">
                                        <h6>{{$i++}}. {{ $company->name }}</h6>
                                    </div>
                                    <div class="float_right">
                                        <a class="modalLink edit_icon" title="Edit Company" data-modal-size="modal-md" href="{{ url('edit_company_modal', $company->id) }}" ><i class="fas fa-edit"></i></a>
                                        <a class="modalLink delete_icon" title="Delete"  data-modal-size="modal-md" href="{{url('deleteCompanyView', $company->id)}}" ><i class="fa fa-trash"></i></a>
                                    </div >
                                </div>

                            @endforeach
                        </div>
                    </div>
                </div>
            </div> --}}


        </div>
    </div>

@endsection
