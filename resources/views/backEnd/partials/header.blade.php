
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="description" content="Gradient Able Bootstrap admin template made using Bootstrap 4 and it has huge amount of ready made feature, UI components, pages which completely fulfills any dashboard needs." />
  <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
  <meta name="author" content="Phoenixcoded" />
  <meta name="baseurl" content="{{url('/')}}">
  <link rel="icon" href="{{asset('public/images/favicon.ico')}}" type="image/x-icon">
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/themify-icons/0.1.2/css/themify-icons.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <!-- Latest compiled and minified CSS -->
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"> -->
  <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/mega_able/css/bootstrap.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/mega_able/css/waves.min.css') }}" type="text/css" media="all">
  <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/mega_able/css/icofont.css') }}">
  <!-- <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/mega_able/css/font-awesome.min.css') }}"> -->
  <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/mega_able/css/prism.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/mega_able/css/style.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/mega_able/css/jquery.mCustomScrollbar.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/mega_able/css/pcoded-horizontal.min.css') }}">

  <!-- For dropdown css -->
  <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/mega_able/css/select2.min.css') }}">

  <!--  start for datatable css -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/assets/mega_able/datatable/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/assets/mega_able/datatable/css/buttons.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/assets/mega_able/datatable/css/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/assets/mega_able/datatable/css/buttons/buttons.dataTables.min.css')}}">
  <!--  end for datatable css -->

  <!-- start added jquery css for datepicker css -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <!-- end added jquery css for datepicker css -->
  <!-- select2 css cdn link -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
  <!-- Custom css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/custom_css.css') }}">
