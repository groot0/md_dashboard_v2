<div class="has-modal modal fade" id="showDetaildModal">
      <div class="modal-dialog modal-dialog-centered" id="modalSize">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="showDetaildModalTile">Modal Title</h4>
				<button type="button" class="close icons" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body" id="showDetaildModalBody">

			</div>

			<!-- Modal footer -->

		</div>
	</div>
</div>

	<!-- Mega able scripts start -->

    <script type="text/javascript" src="{{ asset('public/assets/mega_able/js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/mega_able/js/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/mega_able/js/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/mega_able/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/mega_able/js/waves.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/mega_able/js/jquery.slimscroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/mega_able/js/modernizr.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/mega_able/js/css-scrollbars.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/mega_able/js/custom-prism.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/mega_able/js/i18next.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/mega_able/js/i18nextXHRBackend.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/mega_able/js/i18nextBrowserLanguageDetector.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/mega_able/js/jquery-i18next.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/mega_able/js/pcoded.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/mega_able/js/menu-hori-fixed.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/mega_able/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/mega_able/js/script.js') }}"></script>
	<!-- Mirrored from html.phoenixcoded.net/mega-able/default/menu-horizontal-fixed.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Mar 2019 15:57:35 GMT -->
	<!-- Mega able scripts end -->

    <!-- start for datatable js -->
    <script src="{{asset('public/assets/mega_able/datatable/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('public/assets/mega_able/datatable/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('public/assets/mega_able/datatable/js/jszip.min.js')}}"></script>
    <script src="{{asset('public/assets/mega_able/datatable/js/pdfmake.min.js')}}"></script>
    <script src="{{asset('public/assets/mega_able/datatable/js/vfs_fonts.js')}}"></script>
    <script src="{{asset('public/assets/mega_able/datatable/js/extension/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('public/assets/mega_able/datatable/js/extension/buttons.flash.min.js')}}"></script>
    <script src="{{asset('public/assets/mega_able/datatable/js/extension/jszip.min.js')}}"></script>
    <script src="{{asset('public/assets/mega_able/datatable/js/extension/vfs_fonts.js')}}"></script>
    <script src="{{asset('public/assets/mega_able/datatable/js/extension/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('public/assets/mega_able/datatable/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('public/assets/mega_able/datatable/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('public/assets/mega_able/datatable/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('public/assets/mega_able/datatable/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('public/assets/mega_able/datatable/js/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{asset('public/assets/mega_able/datatable/js/extension/extension-btns-custom.js')}}"></script>
    <!-- end for datatable js -->



    <!-- custom js start-->

<!--     <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous">
    </script> -->
    <script type="text/javascript" src="{{ asset('public/js/eip.js') }}"></script>
    <!-- custom js end-->

    <!-- js cdn for select2 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

<!-- custom js start-->
<script type="text/javascript" src="{{ asset('public/js/custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/searchEmployeeAttendance.js') }}"></script>
<!-- custom js end-->
