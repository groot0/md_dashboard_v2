<div class="pcoded-main-container">
                <nav class="pcoded-navbar">
                    <div class="pcoded-inner-navbar">
                        <ul class="pcoded-item pcoded-left-item">
                            <li class="pcoded-hasmenu">
                                <a href="{{ url('/') }}" class="waves-effect waves-dark">
                                    <span class="pcoded-micon"><i class="ti-pencil-alt"></i><b>F</b></span>
                                    <span class="pcoded-mtext">Task Center</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class="pcoded-hasmenu">
                                <a href="{{ url('mission_and_vision') }}" class="waves-effect waves-dark">
                                    <span class="pcoded-micon"><i class="ti-layout-cta-right"></i><b>N</b></span>
                                    <span class="pcoded-mtext">Mission and Vision</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>

                            </li>
                            <li class="pcoded-hasmenu">
                                <a href="{{ url('goals_and_objective') }}" class="waves-effect waves-dark">
                                    <span class="pcoded-micon"><i class="ti-gift "></i><b>U</b></span>
                                    <span class="pcoded-mtext">Goals and Objectives</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class="pcoded-hasmenu">
                                <a href="{{ url('deliverables_and_deadlines') }}" class="waves-effect waves-dark">
                                    <span class="pcoded-micon"><i class="ti-view-list-alt"></i><b>T</b></span>
                                    <span class="pcoded-mtext">Deliverables and Deadlines</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class="pcoded-hasmenu">
                                <a href="{{ url('performance_and_evaluations') }}" class="waves-effect waves-dark">
                                    <span class="pcoded-micon"><i class="ti-bar-chart-alt"></i><b>C</b></span>
                                    <span class="pcoded-mtext">Performance and Evaluations</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class="pcoded-hasmenu">
                                <a href="{{ url('community_and_social_responsibility') }}" class="waves-effect waves-dark">
                                    <span class="pcoded-micon"><i class="ti-receipt"></i><b>P</b></span>
                                    <span class="pcoded-mtext">Community and Social Responsibility</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class="pcoded-hasmenu">
                                <a href="#!" class="waves-effect waves-dark">
                                    <span class="pcoded-micon"><i class="ti-settings"></i><b>P</b></span>
                                    <span class="pcoded-mtext">Settings</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                                <ul class="pcoded-submenu">
                                    
                                    <li class="">
                                        <a href="{{ url('employee') }}" class="waves-effect waves-dark">
                                            <span class="pcoded-micon"></span>
                                            <span class="pcoded-mtext">Employee</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="{{ url('employee-attendence') }}" class="waves-effect waves-dark">
                                            <span class="pcoded-micon"></span>
                                            <span class="pcoded-mtext">Employee Attendence</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="{{ url('base_group') }}" class="waves-effect waves-dark">
                                            <span class="pcoded-micon"></span>
                                            <span class="pcoded-mtext">Base Group</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="{{ url('base_setup') }}" class="waves-effect waves-dark">
                                            <span class="pcoded-micon"></span>
                                            <span class="pcoded-mtext">Base Setup</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="{{ url('designation') }}" class="waves-effect waves-dark">
                                            <span class="pcoded-micon"></span>
                                            <span class="pcoded-mtext">Designation</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="{{ url('department') }}" class="waves-effect waves-dark">
                                            <span class="pcoded-micon"></span>
                                            <span class="pcoded-mtext">Department</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="{{ url('project') }}" class="waves-effect waves-dark">
                                            <span class="pcoded-micon"></span>
                                            <span class="pcoded-mtext">Projects</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="{{ url('role') }}" class="waves-effect waves-dark">
                                            <span class="pcoded-micon"></span>
                                            <span class="pcoded-mtext">Roles</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="{{ url('user') }}" class="waves-effect waves-dark">
                                            <span class="pcoded-micon"></span>
                                            <span class="pcoded-mtext">Users</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="{{ url('generate_report') }}" class="waves-effect waves-dark">
                                            <span class="pcoded-micon"></span>
                                            <span class="pcoded-mtext">Generate Report</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                    {{-- <li class="">
                                        <a href="{{ url('calculate_hours') }}" class="waves-effect waves-dark">
                                            <span class="pcoded-micon"></span>
                                            <span class="pcoded-mtext">Calculate total hours</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li> --}}

                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div class="pcoded-wrapper">
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <div class="page-body">
                                    	@yield('mainContent')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>