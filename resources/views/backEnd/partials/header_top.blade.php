@if(isset( Auth::user()->name))
    <nav class="navbar header-navbar pcoded-header">
        <div class="navbar-wrapper">
            <div class="navbar-logo">
                <a class="mobile-menu waves-effect waves-light" id="mobile-collapse" href="#!">
                    <i class="ti-menu"></i>
                </a>
                <div class="mobile-search waves-effect waves-light">
                    <div class="header-search">
                        <div class="main-search morphsearch-search">
                            <div class="input-group">
                                <span class="input-group-prepend search-close"><i class="ti-close input-group-text"></i></span>
                                <input type="text" class="form-control" placeholder="Enter Keyword">
                                <span class="input-group-append search-btn"><i class="ti-search input-group-text"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="{{url('/')}}">
                    <img class="img-fluid logo_set" src="{{ asset('public/images/rm_group_img.png') }}" alt="Theme-Logo" />
                </a>
                <a class="mobile-options waves-effect waves-light">
                    <i class="ti-more"></i>
                </a>
            </div>
            <div class="navbar-container container-fluid">
                <ul class="nav-left">
                    <li>
                        <div class="sidebar_toggle"><a href="javascript:void(0)"><i class="ti-menu"></i></a></div>
                    </li>
                    <li class="header-search">
                        <div class="main-search morphsearch-search">
                            <div class="input-group">
                                <span class="input-group-prepend search-close"><i class="ti-close input-group-text"></i></span>
                                <input type="text" class="form-control" placeholder="Enter Keyword">
                                <span class="input-group-append search-btn"><i class="ti-search input-group-text"></i></span>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href="#!" onclick="javascript:toggleFullScreen()" class="waves-effect waves-light">
                            <i class="ti-fullscreen"></i>
                        </a>
                    </li>
                </ul>
                <ul class="nav-right">

                    <li class="user-profile header-notification">
                        <a href="#!" class="waves-effect waves-light">
                            <img src="{{ asset('public/images/md_img.png') }}" class="img-radius" alt="User-Profile-Image">
                            	<span>{{ Auth::user()->name }}</span>
                            <i class="ti-angle-down"></i>
                        </a>
                        <ul class="show-notification profile-notification">
  
                            <li class="waves-effect waves-light">
								<a class="dropdown-item" href="{{ route('logout') }}"" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
									{{ __('Logout') }}
								</a>

								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									@csrf
								</form>
							</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
@else
    <script>
        {{-- https://stackoverflow.com/questions/25203124/how-to-get-base-url-with-jquery-or-javascript --}}
        var getUrl = window.location;
        var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];

        window.location = baseUrl;
    </script>
@endif