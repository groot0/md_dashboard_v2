@extends('backEnd.master')
@section('mainContent')
    <div class="row m-t-40">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h5><i class="icofont icofont-tasks-alt m-r-5"></i>Community</h5>
                </div>
                <div class="card-block">
                    <div class="">
                        <div class="m-b-20">
                            <p>
                                RM promotes and articulates issues of importance to the private sector and seeks to influence policy and to initiate measures crucial to the development of a market-oriented economy as well as sustainable growth of trade, commerce and industry
                            </p>
                            <div class="m-b-20">
                                <p>RM responds to the ever-growing demands of the business and industrial community in a rapidly changing world to address how the private sector in Bangladesh can best cope with and derive the maximum benefit from globalization.
                                </p>
                            </div>
                            <div class="m-b-20">
                                <p>RM recognizes the need for transparent and accountable corporate governance practices in all sectors to lay a solid foundation for the growth of capital markets, to attract foreign direct investment and to encourage indigenous investment.
                                </p>
                            </div>
                            <div class="m-b-20">
                                <p>RM lobbies for the integration of Bangladesh in the global market through unrestricted access for her manufactured goods, especially for ready-made garments in North America, Europe and Asia.
                                To achieve these goals, RM:
                                </p>
                            </div>
                            <div class="m-b-20">
                                <p>Organizes regular consultations, dialogues, seminars and workshops with stakeholders. </p>
                            </div>
                            <div class="m-b-20">
                                <p>Conducts research, surveys and reviews on issues, which are considered to be of vital importance to the private sector.
                                Disseminates knowledge and information on trade, investment, corporate governance and related fields.
                                Formulates policy measures and identifies issues requiring policy intervention for its stakeholders.
                                RM seeks to help the private sector:</p>
                            </div>
                            <div class="m-b-20">
                                <p>Build its capacity to network with national, regional and international bodies which could help to promote the economic interests of Bangladesh.
                                Achieve greater degree of market access for Bangladeshi products abroad.</p>
                            </div>
                            <div class="m-b-20">
                                <p>Influence the policy-making process through effective interaction with decision-makers and legislators.
                                Bangladesh’s recent industrial regime has been characterized by policies seeking to promote the private sector and export-oriented industries. Institutional arrangements and incentives have contributed to the rapid growth of such industries, including successful export-processing zones. This has created both new trade opportunities and challenges for the business community. After the abolition of quota-free acess of ready-made garments with the phasing out of the Multi-Fiber Agreement in January 2005, the country will have to face the challenge of increased competition in both domestic and foreign markets.</p>
                            </div>
                            <div class="m-b-20">
                                <p>Against this backdrop, RM focuses on frontline issues of economic competitiveness that are critically important for Bangladesh industry. RM is aware that, while identifying the implicit and hidden strengths and opportunities, it is necessary to eliminate the many inefficiencies and inadequacies that currently stand in the way of investment. Through the presentation of hard facts and persuasion, RM focuses the key stakeholders on a reform agenda that is realistic and pragmatic.</p>
                            </div>
                            <div class="m-b-20">
                                <p>Domestic issues and concerns arising out of Bangladesh’s effective integration into the process of globalization is another key area of interest to RM. Emerging issues for the domestic economy include: rationalization of the country’s production structure, adoption of improved technologies and enhancement of trade related capacity building. A particular area of interest is the role and growth of SMEs in the domestic economy.</p>
                            </div>
                            <div class="m-b-20">
                                <p>A key to the success of Bangladesh’s export products is open access for Bangladeshi goods to the USA, Canada, India and some other markets. RM provides research and support to lobby for open access and trade liberization. RM also takes an active interest in issues related to regional and sub-regional cooperation in South Asia in areas of trade, investment, environment, transport, security, energy and water resources.
                                Target Group
                                RM seeks to involve and respond to the needs of all sections of society, including business leaders, trade associations, government officials, development partners, academics, professionals and other relevant interest groups in its quest to identify, evolve, advocate and validate policy issues and measures.</p>
                            </div>
                            <div class="m-b-20">
                                <p>RM has received generous support from its Board of Governors. It is now a self-sustaining institute. Funding is provided from projects and research activities. RM’s projects are funded by a wide variety of international multilateral agencies, as well as support from the development agencies of our bilateral partners.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>                      
        </div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h5><i class="icofont icofont-tasks-alt m-r-5"></i>Responsibility</h5>
                </div>
                <div class="card-block">
                    <div class="">
                        <div class="m-b-20">
                            <p>
                                To carry forward the objective of economic development, the Bangladesh Enterprise Institute will continue to enhance the organizational performance of the private sector. Relentless efforts will continue to influence the existing regulatory framework to improve infrastructure and for an investment climate conducive to private sector growth.
                            </p>
                            <ol>
                                <li>Investment Climate Assessment</li>
                                <li>Corporate Governance and Corporate Social Responsibility</li>
                                <li>Administrative Barrier Review</li>
                                <li>E-Governance and e-Commerce</li>
                                <li>Advocacy and Capacity Building</li>
                                <li>Competition Policy</li>
                                <li>Trade Facilitation and Regional Integration</li>
                                <li>SME Development</li>
                                <li>Promoting Accountability, Transparency, and Good Governance in the Government Agencies</li>

                            </ol>
                        </div>
                    </div>
                </div>
            </div>                      
        </div>
    </div>
@endsection