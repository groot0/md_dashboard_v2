@extends('backEnd.master')
@section('mainContent')
    <div class="row m-t-40">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h5><i class="icofont icofont-tasks-alt m-r-5"></i>Generate Report</h5>
                </div>
                <div class="card-block">
                    {{ Form::open(['class' => '', 'files' => true, 'url' => 'search_fields', 'method' => 'POST', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off']) }}
                    <div class="">
                        <div class="m-b-20">
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <select class="form-control {{ $errors->has('department_id') ? ' is-invalid' : '' }}" name="department_id" id="department_id">
                                        <option value="">Select Departmernts</option>
                                        @if(isset($departments))
                                            @foreach($departments as $department)
                                                <option value="{{ $department->id }}" {{ old('department_id')== $department->id ? 'selected' : old('department_id')  }} >{{$department->department_name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control {{ $errors->has('employee_id') ? ' is-invalid' : '' }}" name="employee_id" id="employee_id">
                                        <option value="">Select Employees</option>
                                        @if(isset($employees))
                                            @foreach($employees as $employee)
                                                <option value="{{ $employee->id }}" {{ old('employee_id')== $employee->id ? 'selected' : old('employee_id')  }} >{{$employee->full_name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <input type="" class="form-control datepicker {{ $errors->has('from_date') ? ' is-invalid' : '' }}" value="" name="from_date" placeholder="From Date" />
                                </div>
                                <div class="col-sm-2">
                                    <input type="" class="form-control datepicker {{ $errors->has('to_date') ? ' is-invalid' : '' }}" value="" name="to_date" placeholder="To Date" />
                                </div>
                                
                                <div class="col-sm-2">
                                    <div class="col-sm-12 text-center">
                                        <button type="submit" class="btn btn-primary m-b-0">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{ Form::close()}}
                </div>

                <div class="card-block">
                    
                    @if (isset($results))
                        @php  

                            $i = 1;
                            $set_emp_infos = array();
                            $emp_ids = [];
                            $timestamps = [];
                            $emp_attendes = array();
                            $previous_date = '';
                            $previous_emp_id = '';


                            foreach ($results as $value) {
                                $emp_id_str = (string)$value->employee_id;
                                $datetime = explode(" ",$value->in_time);
                                $present_date = $datetime[0];

                                //Previous code
                                $set_emp_infos = array();
                                if($previous_date != $present_date) {
                                    $emp_ids = [];
                                }
                               
                                if( !in_array($value->employee_id, $emp_ids) ) {
                                    $set_emp_infos['emp_id'] = $value->employee_id;
                                    $set_emp_infos['emp_name'] = $value->employee_name;
                                    $set_emp_infos['dept_name'] = $value->department_name;
                                    $set_emp_infos['dept_id'] = $value->department_id;
                                    if($value->check_type == "I") {
                                        $set_emp_infos['in_time'] = $value->in_time;
                                    } else {
                                        $set_emp_infos['out_time'] = $value->in_time;
                                    }
                                    if(isset($emp_attendes[$emp_id_str]['work_time'])) {
                                        $set_emp_infos['work_time'] = $emp_attendes[$emp_id_str]['work_time'];
                                    }
                                    $emp_attendes[$emp_id_str] = $set_emp_infos;
                                    array_push($emp_ids, $value->employee_id);
                                } else {
                                    if($value->check_type == 'O') {
                                        $emp_attendes[$emp_id_str]['out_time'] = $value->in_time;
                                        $seconds_diff = strtotime($emp_attendes[$emp_id_str]['out_time']) - strtotime($emp_attendes[$emp_id_str]['in_time']);       
                                        $hour = ($seconds_diff/3600);
                                        if(!isset($emp_attendes[$emp_id_str]['work_time'])) {
                                            $emp_attendes[$emp_id_str]['work_time'] = 0.0;
                                        }
                                        if(  isset($emp_attendes[$emp_id_str]['work_time']) && $hour > 5.00 ) {
                                            $emp_attendes[$emp_id_str]['work_time'] = $emp_attendes[$emp_id_str]['work_time'] + $hour;
                                        }                       
                                    }
                                }
                                $datetime = explode(" ",$value->in_time);
                                $previous_date = $datetime[0];
                                $previous_emp_id = (string)$value->employee_id;
                            }

                        @endphp
                    @else
                        <p>No search result found.</p>
                    @endif
                </div>
            </div>                      
        </div>
    </div>
@endsection