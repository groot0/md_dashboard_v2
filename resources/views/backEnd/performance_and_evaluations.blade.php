@extends('backEnd.master')
@section('mainContent')
	<div class="row m-t-40">
        <div class="col-md-12 col-lg-4">
            <div class="card">
                <div class="card-header">
                    <h5>Doughnut Chart</h5>
                    <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                </div>
                <div class="card-block">
                    <canvas id="myChart" width="400" height="400"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-4">
            <div class="card">
                <div class="card-header">
                    <h5>Radar Chart</h5>
                    <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                </div>
                <div class="card-block">
                    <canvas id="radarChart" width="400" height="400"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-4">
            <div class="card">
                <div class="card-header">
                    <h5>Polar Chart</h5>
                    <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                </div>
                <div class="card-block">
                    <canvas id="polarChart" width="400" height="400"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-4">
            <div class="card">
                <div class="card-header">
                    <h5>Pie Chart</h5>
                    <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                </div>
                <div class="card-block">
                    <canvas id="pieChart" width="400" height="400"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-4">
            <div class="card">
                <div class="card-header">
                    <h5>Bar Chart</h5>
                    <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                </div>
                <div class="card-block">
                    <canvas id="barChart" width="400" height="400"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-4">
            <div class="card">
                <div class="card-header">
                    <h5>Bubble Chart</h5>
                    <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                </div>
                <div class="card-block">
                    <canvas id="bubblechart" width="400" height="400"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-4">
            <div class="card">
                <div class="card-header">
                    <h5>Scales Chart</h5>
                    <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                </div>
                <div class="card-block">
                    <canvas id="stancechart" width="400" height="400"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-4">
            <div class="card">
                <div class="card-header">
                    <h5>Time Scale Chart</h5>
                    <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                </div>
                <div class="card-block">
                    <canvas id="timescalechart" width="400" height="400"></canvas>
                </div>
            </div>
        </div>                           
    </div>


        {{-- For google charts in performance and Evalution --}}
        <script type="text/javascript" src="{{ asset('public/assets/mega_able/js/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{asset('public/assets/mega_able/js/Chart.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/assets/mega_able/js/chartjs-custom.js')}}"></script>
        {{-- For google charts --}}

@endsection