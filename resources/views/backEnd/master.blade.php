<!DOCTYPE html>
<html lang="en">
<!-- Mirrored from html.phoenixcoded.net/mega-able/default/menu-horizontal-fixed.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Mar 2019 15:57:34 GMT -->

<head>
    <title>MD Dashboard</title>
    <!--[if lt IE 10]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    @include('backEnd.partials.header')
    @yield('styles')
</head>

<body>
    <div class="theme-loader">
        <div class="loader-track">
            <div class="preloader-wrapper">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="pcoded" class="pcoded">
        <div class="pcoded-container">

        	@include('backEnd.partials.header_top')
        	@include('backEnd.partials.sidebar')


        </div>
    </div>
    <!--[if lt IE 10]>
	<div class="ie-warning">
	    <h1>Warning!!</h1>
	    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers
	        to access this website.</p>
	    <div class="iew-container">
	        <ul class="iew-download">
	            <li>
	                <a href="http://www.google.com/chrome/">
	                    <img src="../files/assets/images/browser/chrome.png" alt="Chrome">
	                    <div>Chrome</div>
	                </a>
	            </li>
	            <li>
	                <a href="https://www.mozilla.org/en-US/firefox/new/">
	                    <img src="../files/assets/images/browser/firefox.png" alt="Firefox">
	                    <div>Firefox</div>
	                </a>
	            </li>
	            <li>
	                <a href="http://www.opera.com">
	                    <img src="../files/assets/images/browser/opera.png" alt="Opera">
	                    <div>Opera</div>
	                </a>
	            </li>
	            <li>
	                <a href="https://www.apple.com/safari/">
	                    <img src="../files/assets/images/browser/safari.png" alt="Safari">
	                    <div>Safari</div>
	                </a>
	            </li>
	            <li>
	                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
	                    <img src="../files/assets/images/browser/ie.png" alt="">
	                    <div>IE (9 & above)</div>
	                </a>
	            </li>
	        </ul>
	    </div>
	    <p>Sorry for the inconvenience!</p>
	</div>
	<![endif]-->
    </body>
	@include('backEnd.partials.footer')
  @yield('scripts')
</html>
