
    <div class="card-block">
		
			{{ Form::open(['class' => '', 'files' => true, 'url' => 'company', 'method' => 'POST', 'autocomplete' => 'off']) }}
	
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
							  	<label for="company_name">Company Name:</label>
							  	<input type="text" id="company_name" required class="form-control {{ $errors->has('company_name') ? ' is-invalid' : '' }}" value="{{ old('company_name') }}" name="name"/>
							  	@if ($errors->has('company_name'))
								    <span class="invalid-feedback" role="alert" >
										<span class="messages"><strong>{{ $errors->first('company_name') }}</strong></span>
									</span>
								@endif
							</div>
							<div class="form-group">
								<label class="company_address" for="company_address">Address: </label>
								<textarea rows="3" cols="3" class="form-control" id="company_address" name="location"></textarea>
								@if ($errors->has('company_address'))
								    <span class="invalid-feedback" role="alert" >
										<span class="messages"><strong>{{ $errors->first('company_address') }}</strong></span>
									</span>
								@endif
							</div>
							<div class="form-group">
								<label class="company_size" for="company_size">Company size: </label>

								<select class="form-control" name="employee_size" id="employee_size">
										<option>Select company</option>
										<option value="1-10">1-10</option>
										<option value="10-50">1-50</option>
										<option value="50-100">50-100</option>
										<option value="100-500">100-500</option>

								</select>
								
							</div>
						</div>

					</div>
				<div class="modal-footer">
	                <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
	                <button type="submit" class="btn btn-primary waves-effect waves-light ">Save</button>
	            </div>

			{{ Form::close()}}
		
	</div>
            