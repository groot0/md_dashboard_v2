@if( isset( $companies ) )
	@foreach($companies as $company)
		<div class="card-block">
			
				{{ Form::open(['class' => '', 'files' => true, 'url' => 'company/'.$company->id, 'method' => 'PUT', 'autocomplete' => 'off']) }}

						<div class="row">
						<div class="col-md-12">
							<div class="form-group">
							  	<label for="company_name">Company Name:</label>
							  	<input type="text" id="company_name" required class="form-control {{ $errors->has('company_name') ? ' is-invalid' : '' }}" value="{{ $company->name }}" name="name"/>
							  	@if ($errors->has('company_name'))
								    <span class="invalid-feedback" role="alert" >
										<span class="messages"><strong>{{ $errors->first('company_name') }}</strong></span>
									</span>
								@endif
							</div>
							<div class="form-group">
								<label class="company_address" for="company_address">Address: </label>
								<textarea rows="3" cols="3" class="form-control" id="company_address" name="location">{{ $company->location }}</textarea>
								@if ($errors->has('company_address'))
								    <span class="invalid-feedback" role="alert" >
										<span class="messages"><strong>{{ $errors->first('company_address') }}</strong></span>
									</span>
								@endif
							</div>
							<div class="form-group">
								<label class="company_size" for="company_size">Company size: </label>

								<select class="form-control" name="employee_size" id="employee_size">
										<option>Select company</option>
										<option value="1-10" @if($company->employee_size === "1-10") selected @endif>1-10</option>
										<option value="10-50" @if($company->employee_size === "10-50") selected @endif>10-50</option>
										<option value="50-100" @if($company->employee_size === "50-100") selected @endif>50-100</option>
										<option value="100-500" @if($company->employee_size === "100-500") selected @endif>100-500</option>

								</select>
								
							</div>
						</div>

					</div>
					<div class="modal-footer">
		                <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
		                <button type="submit" class="btn btn-primary waves-effect waves-light ">Update</button>
		            </div>

				{{ Form::close()}}
			
		</div>
	@endforeach
@endif
            