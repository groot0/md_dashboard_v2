@if( isset( $sectors ) )
	@foreach($sectors as $sector)
        <div class="card-block">
			
				{{ Form::open(['class' => '', 'files' => true, 'url' => 'sector/'.$sector->id, 'method' => 'POST', 'autocomplete' => 'off']) }}

						{{ method_field('PATCH') }}
		
						<div class="row">
						<div class="col-md-12">
							<div class="form-group">
							  	<label for="sector_name">Sector Name:</label>
							  	<input type="text" required id="sector_name" class="form-control {{ $errors->has('sector_name') ? ' is-invalid' : '' }}" value="{{ $sector->name }}" name="sector_name"/>
							  	@if ($errors->has('sector_name'))
								    <span class="invalid-feedback" role="alert" >
										<span class="messages"><strong>{{ $errors->first('sector_name') }}</strong></span>
									</span>
								@endif
							</div>
						</div>

					</div>
					<div class="modal-footer">
		                <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
		                <button type="submit" class="btn btn-primary waves-effect waves-light ">Update</button>
		            </div>

				{{ Form::close()}}
			
		</div>
    @endforeach
@endif