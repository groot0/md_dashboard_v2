<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/attendance_report.css') }}">
  </head>
  <body>
    <h1>Attendance report</h1>
    <h2>Name: {{$employee->full_name}}</h2>
    <h3>From : {{$attendanceFrom}} To: {{$attendanceTo}}</h3>

    <table>
      <thead>
        <tr>
          <th>Date</th>
          <th>In Time</th>
          <th>Out Time</th>
          <th>Daily hours</th>
          <th>Cumulative hours</th>
        </tr>
      </thead>
      <tbody>
        @foreach($attendances['attendances'] as $attendance)

          <tr>
            <td>{{$attendance->attendance_date}}</td>
            <td>{{$attendance->in_time}}</td>
            <td>{{$attendance->out_time}}</td>
            <td>{{$attendance->daily_hours}}</td>
            <td>{{$attendance->cumulative_hours}}</td>
          </tr>

        @endforeach
        <tr>
          <td colspan="4"><b>Total hours: </b></td>
          <td><b>{{$attendances['totalRangeHour']}}</b></td>
        </tr>
      </tbody>
    </table>
  </body>
</html>
