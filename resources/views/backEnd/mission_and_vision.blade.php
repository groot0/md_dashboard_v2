@extends('backEnd.master')
@section('mainContent')
	<div class="row m-t-40">
        {{-- <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h5><i class="icofont icofont-tasks-alt m-r-5"></i>Mission and Vision</h5>
                </div>
                <div class="card-block">
                    <div class="">
                        <div class="m-b-20">
                            <h6 class="sub-title m-b-15">Mission</h6>
                            <p>
                                RM group's Mission is to enrich the quality of life of the people through responsible application of knowledge, technology and skills. RM is committed to the pursuit of excellence through world-class products, innovative processes and empowered employees, to provide the highest level of satisfaction to our customers.
                            </p>
                        </div>
                        <div class="m-b-20">
                            <h6 class="sub-title m-b-15">Vision</h6>
                            <p>
                                To realise the Mission, RM group will :
                            </p>
                        </div>
                        <div class="m-b-20 col-sm-12">
                            <div class="row">
                                <div class="col-md-12 col-lg-6">
                                    <div class="text-primary f-14 m-b-10">
                                        1. Provide products and services of high and consistent quality, ensuring value for money to our customers.
                                    </div>
                                    <div class="text-primary f-14 m-b-10">
                                        2. Endeavour to attain a position of leadership in each category of our businesses.
                                    </div>
                                    <div class="text-primary f-14 m-b-10 m-t-15">
                                        3. Develop our employees by encouraging empowerment and rewarding innovation.
                                    </div>
                                     <div class="text-primary f-14 m-b-10 m-t-15">
                                        4. Promote an environment for learning and personal growth.
                                    </div>
                                    <div class="text-primary f-14 m-b-10 m-t-15">
                                        5. Attain a high level of productivity in all our operations through effective utilisation of resources and adoption of appropriate technology.
                                    </div>
                                    <div class="text-primary f-14 m-b-10 m-t-15">
                                        6. Ensure superior return on investment through judicious use of resources and efficient operations, utilising our core competencies.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-b-20">
                            <h6 class="sub-title m-b-15">Values</h6>
                            
                        </div>
                        <div class="m-b-20 col-sm-12">
                            <div class="row">
                                <div class="col-md-12 col-lg-6">
                                    <ol>
                                        <li>Quality</li>
                                        <li>Customer Focus</li>
                                        <li>Innovation</li>
                                        <li>Fairness</li>
                                        <li>Transparency</li>
                                        <li>Continuous Improvement</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>                      
        </div> --}}

        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h5><i class="icofont icofont-tasks-alt m-r-5"></i>Mission and Vision</h5>
                </div>
                <div class="card-block">
                    <div class="">
                        <div class="m-b-20">
                            <h6 class="sub-title m-b-15">Mysql Data</h6>
                            <p>
                                <div>
                                </div>
                                @if (isset($companies))
                                    @foreach($companies as $company)
                                        <p>{{$company->name}}</p> 
                                    @endforeach
                                @endif
                            </p>
                        </div>
                        <div class="m-b-20">
                            <h6 class="sub-title m-b-15">MS Access data</h6>
        
                        </div>
                        <div class="m-b-20 col-sm-12">
                            <div class="row">
                                @php
                                    $bits = 8 * PHP_INT_SIZE;

                                    $connStr = 
                                            'odbc:Driver={Microsoft Access Driver (*.mdb, *.accdb)};' .
                                            'Dbq=C:\\Users\\USER\\Downloads\\Microsoft.SkypeApp_kzf8qxf38zg5c!App\\All\\att2001.mdb;';

                                    try {
                                        $dbh = new PDO($connStr);
                                        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                                        $sql = "SELECT * FROM USERINFO";
                                        $sth = $dbh->prepare($sql);

                                        // query parameter value(s)
                                        $params = array(
                                                5,
                                                'Homer'
                                                );

                                        $sth->execute($params);

                                        while ($row = $sth->fetch()) {
                                            echo $row['Name'] . "<br>";
                                        }
                                    } catch (PDOException $e) {
                                        echo 'Please check your file path.';
                                    }
                                    
                                @endphp
                            </div>
                        </div>
                    </div>
                </div>
            </div>                      
        </div> 

    </div>
@endsection