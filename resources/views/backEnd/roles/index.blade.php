@extends('backEnd.master')
@section('mainContent')

<div class="container-fluid container_margin">
	<div class="row">
		<div class="col-md-4">
			@if(session()->has('message-success'))
				<div class="alert alert-success mb-3 background-success" role="alert">
					{{ session()->get('message-success') }}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			@elseif(session()->has('message-danger'))
				<div class="alert alert-danger">
					{{ session()->get('message-danger') }}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			@endif
			@if(session()->has('message-success-delete'))
				<div class="alert alert-danger mb-3 background-danger" role="alert">
					{{ session()->get('message-success-delete') }}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			@elseif(session()->has('message-danger-delete'))
				<div class="alert alert-danger">
					{{ session()->get('message-danger-delete') }}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			@endif

			<div class="card">
				<div class="card-header">
					<h5>Add New Role</h5>
				</div>
				<div class="card-block">
					@if (isset($editData))
						{{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'role/'.$editData->id, 'method' => 'PUT', 'enctype' => 'multipart/form-data']) }}
					@else
						{{ Form::open(['class' => '', 'files' => true, 'url' => 'role','method' => 'POST', 'enctype' => 'multipart/form-data']) }}
					@endif
					
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="col-form-label">Role Name</label>
									<input type="text" class="form-control {{ $errors->has('role_name') ? ' is-invalid' : '' }}" name="role_name" id="name" placeholder="Add new role"  @if (isset($editData))
										value="{{ $editData->role_name }}" @else value="{{old('role_name')}}"
									@endif autocomplete="off">

									@if ($errors->has('role_name'))
									<span class="invalid-feedback" role="alert">
										<span class="messages"><strong>{{ $errors->first('role_name') }}</strong></span>
									</span>
									@endif
								</div>
							</div>
						</div>
						<div class="form-group row mt-4">
							<div class="col-sm-12 text-center">
								<button type="submit" class="btn btn-primary m-b-0">Submit</button>
							</div>
						</div>
					{{ Form::close()}}
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<h5>Roles</h5>
				</div>
				<div class="card-block">
					<div class="dt-responsive table-responsive">
					<table id="basic-btn" class="table table-striped table-bordered nowrap basic-btn">
						<thead>
							<tr>
								<th>Serial</th>
								<th>Role Name</th>
								<th>Status</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							@if(isset($roles))
								@php $i = 1 @endphp
								@foreach($roles as $role)
								<tr>
									<td>{{$i++}}</td>
									<td>{{$role->role_name}}</td>
									<td><button type="button" class="btn btn-success btn-sm">Active</button></td>
									<td>
										
										<a href="{{ route('role.edit',$role->id) }}" class="edit_icon" title="edit"><i class="fa fa-edit"></i></a>
										<a class="modalLink delete_icon" title="Delete Role" data-modal-size="modal-md" href="{{url('deleteRoleView', $role->id)}}">
											<i class="fa fa-trash"></i>
										</a>
										{{-- <a href="{{url('assign-permission', $role->id)}}" title="view"><button type="button" class="btn btn-success action-icon">Assign Permission</button></a> --}}
								    </td>
								</tr>
								@endforeach
							@endif
						</tbody>
					</table>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endSection