@extends('backEnd.master')
@section('mainContent')
	
<div class="container-fluid container_margin">
	<div class="card">
		<div class="card-header">
			<h5>Edit Project</h5>
		</div>
		<div class="card-block">
			 <form method="post" action="{{ route('project.update', $editData->id) }}">
			    @method('PATCH')
			    @csrf
			    <div class="row">
				    <div class="form-group col-md-6">
					  	<label for="project_name">Project Name:</label>
					  	<input type="text" class="form-control {{ $errors->has('project_name') ? ' is-invalid' : '' }}" name="project_name" value="{{$editData->project_name }}" />
					  	@if ($errors->has('project_name'))
						    <span class="invalid-feedback" role="alert" >
								<span class="messages"><strong>{{ $errors->first('project_name') }}</strong></span>
							</span>
						@endif
					</div>
					<div class="form-group col-md-6">
					  	<label for="project_start_date">Project Start Date :</label>
					  	<input type="" class="form-control datepicker {{ $errors->has('project_start_date') ? ' is-invalid' : '' }}" name="project_start_date" 
					  	@if(isset($editData->project_start_date)) 
					  		value="{{ date('d-m-Y', strtotime($editData->project_start_date)) }}"
					  	@endif
					  	name="project_start_date"/>

					  	@if ($errors->has('project_start_date'))
						    <span class="invalid-feedback" role="alert" >
								<span class="messages"><strong>{{ $errors->first('project_start_date') }}</strong></span>
							</span>
						@endif
					</div>
			    </div>

				<div class="row">	
					<div class="form-group col-md-6">
					  	<label for="project_end_date">Project End Date :</label>
					  	<input type="" class="form-control datepicker {{ $errors->has('project_end_date') ? ' is-invalid' : '' }}" name="project_end_date" 
					  	@if(isset($editData->project_end_date)) 
					  		value="{{ date('d-m-Y', strtotime($editData->project_end_date)) }}"
					  	@endif
					  	name="project_end_date"/>

					  	@if ($errors->has('project_end_date'))
						    <span class="invalid-feedback" role="alert" >
								<span class="messages"><strong>{{ $errors->first('project_end_date') }}</strong></span>
							</span>
						@endif
					</div>
					<div class="form-group col-md-6">
					  	<label for="project_amount">Project Amount:</label>
					  	<input type="number" class="form-control  {{ $errors->has('project_amount') ? ' is-invalid' : '' }}" name="project_amount" value="{{ $editData->project_amount }}"/>
					  	@if ($errors->has('project_amount'))
						    <span class="invalid-feedback" role="alert" >
								<span class="messages"><strong>{{ $errors->first('project_amount') }}</strong></span>
							</span>
						@endif
					</div>
				</div>
				
				<div class="row">
					<div class="form-group col-md-6">
					  	<label class="col-form-label">Client ID:</label>
						<select class="form-control" name="client_id" id="client_id">
						    <option value="">Select client name</option>
						    @if(isset($clients))
							@foreach($clients as $key=>$value)
							<option value="{{$value->id}}"
								@if(isset($editData))
									@if($editData->client_id == $value->id)
										selected
									@endif
								@endif
								>{{$value->client_name}}
							</option>
								@endforeach
								@endif
						</select>

						@if ($errors->has('client_id'))
						<span class="invalid-feedback invalid-select" role="alert">
							<span class="messages"><strong>{{ $errors->first('client_id') }}</strong></span>
						</span>
						@endif

					</div>
					<div class="form-group col-md-6">
					  <label for="advances_received">Advances Received:</label>
					  <input type="number" class="form-control" name="advances_received" value="{{ $editData->advances_received }}"/>
					</div>				
				</div>
				<div class="row">
					<div class="form-group col-md-6">
					  	<label for="last_date_of_receipt">Last Date of Receipt:</label>
					  	<input type="" class="form-control datepicker" name="last_date_of_receipt" 
					  	@if(isset($editData->last_date_of_receipt)) 
					  		value="{{ date('d-m-Y', strtotime($editData->last_date_of_receipt)) }}"
					  	@endif
					  	name="last_date_of_receipt"/>
					  	@if ($errors->has('last_date_of_receipt'))
						    <span class="invalid-feedback" role="alert" >
								<span class="messages"><strong>{{ $errors->first('last_date_of_receipt') }}</strong></span>
							</span>
						@endif
					</div>
					<div class="form-group col-md-6">
					  <label for="completion_due_date">Completion Due Date:</label>
					  <input type="" class="form-control datepicker" name="completion_due_date" 
					  	@if(isset($editData->completion_due_date)) 
					  		value="{{ date('d-m-Y', strtotime($editData->completion_due_date)) }}"
					  	@endif
					  	name="completion_due_date"/>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-6">
					  <label for="completed_on">Completed on:</label>
					  <input type="" class="form-control datepicker" name="completed_on" 
					  	@if(isset($editData->completed_on)) 
					  		value="{{ date('d-m-Y', strtotime($editData->completed_on)) }}"
					  	@endif
					  	name="completed_on"/>
					</div>
					<div class="form-group col-md-6">
					  <label for="receipts_to_date">Receipts to Date:</label>
					  <input type="number" class="form-control" name="receipts_to_date" value="{{ $editData->receipts_to_date }}"/>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-6">
					  <label for="expenses_to_date">Expenses to Date:</label>
					  <input type="number" class="form-control" name="expenses_to_date" value="{{ $editData->expenses_to_date }}"/>
					</div>
				</div>

			    <div class="form-group row mt-5">
					<div class="col-sm-12 text-center">
						<button type="submit" class="btn btn-primary m-b-0">Update</button>
					</div>
				</div>
			  </form>

		</div>
	</div>
</div>

@endSection