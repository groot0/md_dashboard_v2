<div class="row">
    <div class="card-header-img text-center" style="text-align: center; margin: 0 auto;" >
       <div style="text-align: center; margin-bottom: 20px;">
            <button type="button" class="btn btn-primary waves-effect waves-light m-r-15">
            	@if(isset($employee_name))
            		{{$employee_name->name}}
            	@endif
            </button>
       </div>
   </div>
</div>

<div class="dt-responsive table-responsive">
    <table id="" class="table table-striped table-bordered nowrap">
        <thead>
            <tr>
                <th>In Time</th>
                <th>Out Time</th>
            </tr>
        </thead>
        <tbody> 

            @php
                $i = 0;
                $in_arr = [];
                $out_arr = [];
                foreach($results as $value) {
                    if(count($results) == 1 && $value->check_type == 'I') {
                        echo '<tr>';
                            echo '<td>'.date('h:i a', strtotime($value->in_time)).'</td>';
                            echo '<td></td>';
                        echo '<tr>';
                    } else if(count($results) == 1 && $value->check_type == 'O') {
                        echo '<tr>';
                            echo '<td></td>';
                            echo '<td>'.date('h:i a', strtotime($value->in_time)).'</td>';
                        echo '<tr>';
                    } else {
                        if($value->check_type == 'I') {
                            $in_time = date('h:i a', strtotime($value->in_time));
                            array_push($in_arr, $in_time);
                        } if($value->check_type == 'O') {
                            $out_time = date('h:i a', strtotime($value->in_time));
                            if(count($in_arr) > 0) {
                                echo '<tr>';
                                    echo '<td>'.$in_arr[0].'</td>';
                                    echo '<td>'.$out_time.'</td>';
                                echo '<tr>';
                            }
                            
                            $in_arr = [];
                        }
                        $i = $i + 1;
                    }
                    
                }
            @endphp

        </tbody>
    </table>
</div>







