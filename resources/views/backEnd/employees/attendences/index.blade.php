@extends('backEnd.master')
@section('mainContent')
<div class="container-fluid container_margin">
	<div class="row">
	 <div class="col-md-12">
		 <div class="filter-attendance-form col-md-4">
			 <div class="employee-select-container">
				 <label for="select_employee">Employee name: </label>
				 <select class="js-example-basic-single" id="select_employee" name="employee_name">
					 @foreach($departments as $department)

					 	<optgroup label="{{$department->department_name}}">
							@foreach($department->employees as $employee)

							<option value={{$employee->id}}>{{$employee->full_name}}</option>
							@endforeach

						</optgroup>

					 @endforeach
					</select>
			 </div>
			 <div>
				 <p class="date-picker-from">From date: <input type="text" id="date-picker-from"></p>
				 <p class="date-picker-to">To date: <input type="text" id="date-picker-to"></p>
			 </div>
			 <button id="search_attendance_btn" class="btn btn-primary btn-sm">Search</button>

			 <button class="btn btn-warning btn-sm" id="export_pdf">Export PDF</button>
		 </div>
		<div class="card">
			<div class="card-header">
				<h5>Attendence Report</h5>
			</div>
			<div class="card-block">
				<div class="dt-responsive table-responsive">
					<table id="attendanceTable" class="table table-striped table-bordered nowrap">
						<thead>
							<tr>
								<th>Date</th>
								<th>In Time</th>
								<th>Out Time</th>
								<th>Daily hours</th>
								<th>Cumulative hours</th>
							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

</div>

@endSection
