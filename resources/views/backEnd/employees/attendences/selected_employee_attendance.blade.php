@extends('backEnd.master')
@section('styles')
  <link rel="stylesheet" href="{{asset('public/css/selected_attendance.css')}}">
@endsection
@section('mainContent')

<div id="selectedAttendanceTable" class="container" style="margin-top: 40px">

</div>

@section('scripts')

  <script type="text/javascript" src="{{asset('public/js/selected_attendance.js')}}"></script>

@endsection
@endsection
