@extends('backEnd.master')
@section('mainContent')

{{-- This codes are for formatting emp_attendence to date wise in_time and out_time start --}}
@php
	$dates = [];
@endphp

@if (isset($employee_attendences))
	@foreach ($employee_attendences as $employee_attendence)
		@php

			$get_in_time = $employee_attendence['in_time'];
			$dt = new DateTime($get_in_time);
			$date = $dt->format('Y-m-d');
			$time = $dt->format('H:i:s');
			if (!in_array($date, $dates)) {
				array_push($dates, $date);
				$date_arr[$date] = [];
			}
			if($employee_attendence['check_type'] == 'I') {
				if (!isset($date_arr[$date]['in_time'])) {
					$date_arr[$date]['in_time'] = $get_in_time;
				}
			} if($employee_attendence['check_type'] == 'O') {
				$date_arr[$date]['out_time'] = $get_in_time;
			}
	
		@endphp
	@endforeach	
@endif
{{-- This codes are for formatting emp_attendence to date wise in_time and out_time end --}}

@if (isset($employee))
	<div class="container-fluid container_margin">
		<div class="card">
			@if (session('message-danger'))
		      <div class="alert alert-danger">
		        <ul>
		            <li>{{ session('message-danger') }}</li>
		        </ul>
		      </div><br />
		    @endif

		    @if (session('message-success'))
		      <div class="alert alert-success">
		        <ul>
		            <li>{{ session('message-success') }}</li>
		        </ul>
		      </div><br />
		    @endif

		    {{-- Just form start --}}
			<div class="card-header">
				<h5>Adjust Employee Attendance</h5>
			</div>
			<div class="card-block">
				{{ Form::open(['class' => '', 'files' => true, 'url' => 'employee_time_adjust/'.$employee->device_USERID, 'method' => 'PUT', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off']) }}
					<div class="margin_bottom_18">
						<span >Employee Name:<span class="bold"> {{$employee->full_name}}</span></span>
					</div>
					
					<div class="row">
						<input type="text" name="emp_full_name" value="{{$employee->full_name}}" style="display: none;">
						<div class="form-group col-md-3">
							<label for="date">Date:</label>
							<input type="" class="form-control datepicker" value="" name="date"/>
							@if ($errors->has('date'))
							<span class="invalid-feedback" role="alert" >
								<span class="messages"><strong>{{ $errors->first('date') }}</strong></span>
							</span>
							@endif
						</div>
						<div class="form-group col-md-3">
							<label for="in_time">In Time:</label>
							<input type="time" class="form-control" value="" name="in_time"/>
							@if ($errors->has('in_time'))
							<span class="invalid-feedback" role="alert" >
								<span class="messages"><strong>{{ $errors->first('in_time') }}</strong></span>
							</span>
							@endif
						</div>

						<div class="form-group col-md-3">
							<label for="out_time">Out Time:</label>
							<input type="time" class="form-control" value="" name="out_time"/>
							@if ($errors->has('out_time'))
							<span class="invalid-feedback" role="alert" >
								<span class="messages"><strong>{{ $errors->first('out_time') }}</strong></span>
							</span>
							@endif
						</div>

						<div class="form-group col-md-3">
							<div class="col-sm-12 text-center margin_top_22">
								<button type="submit" class="btn btn-primary m-b-0">Save</button>
							</div>
						</div>

					</div>
				{{ Form::close()}}
			</div>
			{{-- Just form end --}}

			{{-- This section for showing datatable start --}}
			<div class="card-block">
				<div class="dt-responsive table-responsive">
					<table id="basic-btn" class="table table-striped table-bordered nowrap basic-btn">
						<thead>
							<tr>
								<th>Date</th>
								<th>In Time</th>
								<th>Out Time</th>
							</tr>
						</thead>
						<tbody>
							@if (isset($date_arr))
								@foreach ($date_arr as $key=>$value)
									<tr>
										@if (isset($key))
											<td>@php echo date('d-m-Y', strtotime($key)); @endphp</td>
										@else
											<td></td>
										@endif

										@if (isset($value['in_time']))
											<td>@php echo date('h:i A', strtotime($value['in_time'])); @endphp</td>
										@else
											<td></td>
										@endif

										@if (isset($value['out_time']))
											<td>@php echo date('h:i A', strtotime($value['out_time'])); @endphp</td>
										@else
											<td></td>
										@endif
									</tr>
								@endforeach
							@endif
						</tbody>
					</table>
				</div>
			</div>
			{{-- This section for showing datatable end --}}
		</div>
	</div>
@endif

@endSection
