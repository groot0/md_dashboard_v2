@extends('backEnd.master')
@section('mainContent')

<div class="container-fluid container_margin">
	<div class="card">
		@if (session('message-danger-appo'))
		<div class="alert alert-danger">
			<ul>
				<li>{{ session('message-danger-appo') }}</li>
			</ul>
		</div><br />
		@endif

		@if (session('message-danger-cv'))
		<div class="alert alert-danger">
			<ul>
				<li>{{ session('message-danger-cv') }}</li>
			</ul>
		</div><br />
		@endif

		@if (session('message-danger-img'))
		<div class="alert alert-danger">
			<ul>
				<li>{{ session('message-danger-img') }}</li>
			</ul>
		</div><br />
		@endif
		
		@if(session()->has('message-danger'))
		<div class="alert alert-danger">
			{{ session()->get('message-danger') }}
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		@endif
		<div class="card-header">
			@if(isset($showOnly))
				<h5>Employee Details</h5>
			@else
				<h5>Edit Employee</h5>
			@endif
		</div>
		<div class="card-block">
			{{ Form::open(['class' => '', 'files' => true, 'url' => 'employee/'.$editData->id, 'method' => 'PUT', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off']) }}

			<div class="row">
				<div class="form-group col-md-6">

				</div>

				<div class="form-group col-md-6">
					@if( isset($editData->employee_photo) )
					<img src="{{ asset($editData->employee_photo) }}" class="image_center img-radius" height="200px" width="200px" />
					@else
					<img src="{{ asset('/public/images/no_image.png') }}" class="image_center img-radius" height="200px" width="200px" />
					@endif
				</div>
			</div>


			<div class="row">
				<div class="form-group col-md-3">
					<label for="user_defined_employee_id">Employee ID:</label>
					<input @if(isset($showOnly)) readonly @endif type="text" class="form-control  {{ $errors->has('user_defined_employee_id') ? ' is-invalid' : '' }}" value="{{$editData->ud_employee_id}}" name="user_defined_employee_id" />
					@if ($errors->has('user_defined_employee_id'))
					<span class="invalid-feedback" role="alert" >
						<span class="messages"><strong>{{ $errors->first('user_defined_employee_id') }}</strong></span>
					</span>
					@endif
				</div>
				<div class="form-group col-md-3">
					<label for="device_USERID">Device User ID:</label>
					<input @if(isset($showOnly)) readonly @endif type="text" class="form-control  {{ $errors->has('device_USERID') ? ' is-invalid' : '' }}" value="{{$editData->device_USERID}}" name="device_USERID" />
					@if ($errors->has('device_USERID'))
					<span class="invalid-feedback" role="alert" >
						<span class="messages"><strong>{{ $errors->first('device_USERID') }}</strong></span>
					</span>
					@endif
				</div>

				<div class="form-group col-md-3">
					<label for="first_name">First Name:</label>
					<input @if(isset($showOnly)) readonly @endif type="text" class="form-control  {{ $errors->has('first_name') ? ' is-invalid' : '' }}" value="{{$editData->first_name}}" name="first_name" />
					@if ($errors->has('first_name'))
					<span class="invalid-feedback" role="alert" >
						<span class="messages"><strong>{{ $errors->first('first_name') }}</strong></span>
					</span>
					@endif
				</div>

				<div class="form-group col-md-3">
					<label for="last_name">Last Name:</label>
					<input @if(isset($showOnly)) readonly @endif type="text" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" value="{{$editData->last_name}}" name="last_name"/>
					@if ($errors->has('last_name'))
					<span class="invalid-feedback" role="alert" >
						<span class="messages"><strong>{{ $errors->first('last_name') }}</strong></span>
					</span>
					@endif
				</div>

			</div>

			<div class="row">

				<div class="form-group col-md-3">
					<label for="email">Email:</label>
					<input @if(isset($showOnly)) readonly @endif type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{$editData->email}}" name="email"/>
					@if ($errors->has('email'))
					<span class="invalid-feedback" role="alert" >
						<span class="messages"><strong>{{ $errors->first('email') }}</strong></span>
					</span>
					@endif
				</div>

				<div class="form-group col-md-3">
					<label for="mobile">Cell no:</label>
					<input type="text" @if(isset($showOnly)) readonly @endif class="form-control {{ $errors->has('mobile') ? ' is-invalid' : '' }}" value="{{$editData->mobile}}" name="mobile"/>
					@if ($errors->has('mobile'))
					<span class="invalid-feedback" role="alert" >
						<span class="messages"><strong>{{ $errors->first('mobile') }}</strong></span>
					</span>
					@endif
				</div>

				<div class="form-group col-md-3">
					<label for="employee_type_id">Employee type:</label>
					<select @if(isset($showOnly)) disabled @endif class="form-control {{ $errors->has('employee_type_id') ? ' is-invalid' : '' }}" name="employee_type_id" id="employee_type_id">
						<option value="">Select Type</option>
						@if(isset($employee_types))
						@foreach($employee_types as $key=>$value)
						<option value="{{$value->id}}"
							@if(isset($editData))
							@if($editData->employee_type_id == $value->id)
							selected
							@endif
							@endif
							>{{$value->base_setup_name}}
						</option>
						@endforeach
						@endif
					</select>
					@if ($errors->has('employee_type_id'))
					<span class="invalid-feedback invalid-select" role="alert">
						<strong>{{ $errors->first('employee_type_id') }}</strong>
					</span>
					@endif
				</div>
				@if(!isset($showOnly))
				<div class="form-group col-md-3">
					<label for="employee_photo">Employee Image:</label><br>
					<input data-preview="#preview" name="employee_photo" type="file" id="employee_photo">

					@if ($errors->has('employee_photo'))
					<span class="invalid-feedback" role="alert" >
						<span class="messages"><strong>{{ $errors->first('employee_photo') }}</strong></span>
					</span>
					@endif
				</div>
				@endif

			</div>

			<div class="row">
				<div class="form-group col-md-2">
					<label for="emergency_no">Emergency Contact:</label>
					<input type="text" @if(isset($showOnly)) readonly @endif class="form-control {{ $errors->has('emergency_no') ? ' is-invalid' : '' }}" value="{{$editData->emergency_no }}" name="emergency_no"/>
					@if ($errors->has('emergency_no'))
					<span class="invalid-feedback" role="alert" >
						<span class="messages"><strong>{{ $errors->first('emergency_no') }}</strong></span>
					</span>
					@endif
				</div>
				<div class="form-group col-md-2">
					<label for="emergency_contact_name">Emergency Contact Name:</label>
					<input type="text" @if(isset($showOnly)) readonly @endif class="form-control {{ $errors->has('emergency_contact_name') ? ' is-invalid' : '' }}" value="{{$editData->emergency_contact_name}}" name="emergency_contact_name"/>
					@if ($errors->has('emergency_contact_name'))
					<span class="invalid-feedback" role="alert" >
						<span class="messages"><strong>{{ $errors->first('emergency_contact_name') }}</strong></span>
					</span>
					@endif
				</div>
				<div class="form-group col-md-2">
					<label for="emergency_contact_relation">Emergency Contact Relation:</label>
					<input type="text" @if(isset($showOnly)) readonly @endif class="form-control {{ $errors->has('emergency_contact_relation') ? ' is-invalid' : '' }}" value="{{$editData->emergency_contact_relation}}" name="emergency_contact_relation"/>
					@if ($errors->has('emergency_contact_relation'))
					<span class="invalid-feedback" role="alert" >
						<span class="messages"><strong>{{ $errors->first('emergency_contact_relation') }}</strong></span>
					</span>
					@endif
				</div>

				<div class="form-group col-md-3">
					<label for="date_of_birth">Date of birth:</label>
					<input @if(isset($showOnly)) readonly @endif type="" class="form-control datepicker {{ $errors->has('date_of_birth') ? ' is-invalid' : '' }}"
					@if(isset($editData->date_of_birth))
					value="{{ date('d-m-Y', strtotime($editData->date_of_birth)) }}"
					@endif
					name="date_of_birth"/>
					@if ($errors->has('date_of_birth'))
					<span class="invalid-feedback" role="alert" >
						<span class="messages"><strong>{{ $errors->first('date_of_birth') }}</strong></span>
					</span>
					@endif
				</div>

				<div class="form-group col-md-3">
					<label for="joining_date">Joining date:</label>
					<input @if(isset($showOnly)) readonly @endif type="" class="form-control datepicker {{ $errors->has('joining_date') ? ' is-invalid' : '' }}"
					@if(isset($editData->date_of_birth))
					value="{{ date('d-m-Y', strtotime($editData->joining_date)) }}"
					@endif
					name="joining_date"/>
					@if ($errors->has('joining_date'))
					<span class="invalid-feedback" role="alert" >
						<span class="messages"><strong>{{ $errors->first('joining_date') }}</strong></span>
					</span>
					@endif
				</div>

			</div>


			<div class="row">

				<div class="form-group col-md-3">
					<label for="department_id">Department name:</label>
					<select @if(isset($showOnly)) disabled @endif class="form-control {{ $errors->has('department_id') ? ' is-invalid' : '' }}" name="department_id" id="department_id">
						<option value="">Select Dept.</option>
						@if(isset($departments))
						@foreach($departments as $key=>$value)
						<option value="{{$value->id}}"
							@if(isset($editData))
							@if($editData->department_id == $value->id)
							selected
							@endif
							@endif
							>{{$value->department_name}}
						</option>
						@endforeach
						@endif
					</select>
					@if ($errors->has('department_id'))
					<span class="invalid-feedback invalid-select" role="alert">
						<strong>{{ $errors->first('department_id') }}</strong>
					</span>
					@endif
				</div>

				<div class="form-group col-md-3">
					<label for="designation_id">Designation name:</label>
					<select @if(isset($showOnly)) disabled @endif class="form-control {{ $errors->has('designation_id') ? ' is-invalid' : '' }}" name="designation_id" id="designation_id">
						<option value="">Select Designation</option>
						@if(isset($designations))
						@foreach($designations as $key=>$value)
						<option value="{{$value->id}}"
							@if(isset($editData))
							@if($editData->designation_id == $value->id)
							selected
							@endif
							@endif
							>{{$value->designation_name}}
						</option>
						@endforeach
						@endif
					</select>
					@if ($errors->has('designation_id'))
					<span class="invalid-feedback invalid-select" role="alert">
						<strong>{{ $errors->first('designation_id') }}</strong>
					</span>
					@endif
				</div>

				<div class="form-group col-md-3">
					<label for="gender_id">Gender:</label>
					<select @if(isset($showOnly)) disabled @endif class="form-control {{ $errors->has('gender_id') ? ' is-invalid' : '' }}" name="gender_id" id="gender_id">
						<option value="">Select gender</option>
						@if(isset($genders))
						@foreach($genders as $key=>$value)
						<option value="{{$value->id}}"
							@if(isset($editData))
							@if($editData->gender_id == $value->id)
							selected
							@endif
							@endif
							>{{$value->base_setup_name}}
						</option>
						@endforeach
						@endif
					</select>
					@if ($errors->has('gender_id'))
					<span class="invalid-feedback invalid-select" role="alert">
						<strong>{{ $errors->first('gender_id') }}</strong>
					</span>
					@endif
				</div>

				<div class="form-group col-md-3">
					<label for="blood_group_id">Blood group:</label>
					<select @if(isset($showOnly)) disabled @endif class="form-control {{ $errors->has('blood_group_id') ? ' is-invalid' : '' }}" name="blood_group_id" id="blood_group_id">
						<option value="">Select group</option>
						@if(isset($blood_groups))
						@foreach($blood_groups as $key=>$value)
						<option value="{{$value->id}}"
							@if(isset($editData))
							@if($editData->blood_group_id == $value->id)
							selected
							@endif
							@endif
							>{{$value->base_setup_name}}
						</option>
						@endforeach
						@endif
					</select>
					@if ($errors->has('blood_group_id'))
					<span class="invalid-feedback invalid-select" role="alert">
						<strong>{{ $errors->first('blood_group_id') }}</strong>
					</span>
					@endif
				</div>

			</div>

			<div class="row">

				<div class="form-group col-md-6">
					<label for="permanent_address">Permanent address:</label>
					<textarea rows="4" @if(isset($showOnly)) readonly @endif class="form-control" value="" name="permanent_address">{{  $editData->permanent_address }}</textarea>
				</div>

				<div class="form-group col-md-6">
					<label for="current_address">Current address:</label>
					<textarea rows="4" @if(isset($showOnly)) readonly @endif class="form-control" value="" name="current_address">{{  $editData->current_address }}</textarea>
				</div>

			</div>

			<div class="row">

				<div class="form-group col-md-6">
					<label for="qualifications">Qualifications:</label>
					<textarea rows="10" @if(isset($showOnly)) readonly @endif class="form-control" value="" name="qualifications">{{  $editData->qualifications }}</textarea>
				</div>

				<div class="form-group col-md-6">
					<label for="experiences">Experiences:</label>
					<textarea rows="10" @if(isset($showOnly)) readonly @endif class="form-control" value="" name="experiences">{{  $editData->experiences }}</textarea>
				</div>

			</div>

			<div class="row">

				<div class="form-group col-md-4">
					<label for="job_description">Job Description:</label>
					<textarea rows="10" @if(isset($showOnly)) readonly @endif class="form-control" value="{{ old('job_description') }}" name="job_description">{{  $editData->job_description }}</textarea>
				</div>

				@if(!isset($showOnly))
				<div class="form-group col-md-4">
					<label for="employee_cv">Resume:(Max Size: 10 MB)</label><br>
					<input data-preview="#preview" name="employee_cv" type="file" id="employee_cv">
					@if (isset($editData->employee_cv))
					<a href="{{ asset($editData->employee_cv) }}" target="_blank" >View/Download</a>
					@endif
					@if ($errors->has('employee_cv'))
					<span class="invalid-feedback" role="alert" >
						<span class="messages"><strong>{{ $errors->first('employee_cv') }}</strong></span>
					</span>
					@endif
				</div>
				@elseif(isset($editData->employee_cv))
				<div>
					<label>Resume: </label><br>
					<a href="{{ asset($editData->employee_cv) }}" target="_blank" >View/Download</a>
				</div>
				@endif

				@if(!isset($showOnly))
				<div class="form-group col-md-4">
					<label for="employee_appointment_letter">Appointment letter:(Max Size: 10 MB)</label><br>
					<input @if(isset($showOnly)) readonly @endif data-preview="#preview" name="employee_appointment_letter" type="file" id="employee_appointment_letter">
					@if (isset($editData->employee_appointment_letter))
					<a href="{{ asset($editData->employee_appointment_letter) }}" target="_blank" >View/Download</a>
					@endif
					@if ($errors->has('employee_appointment_letter'))
					<span class="invalid-feedback" role="alert" >
						<span class="messages"><strong>{{ $errors->first('employee_appointment_letter') }}</strong></span>
					</span>
					@endif
				</div>
				@elseif(isset($editData->employee_appointment_letter))
				<div>
					<label for="employee_appointment_letter">Appointment letter</label><br>
					<a href="{{ asset($editData->employee_appointment_letter) }}" target="_blank" >View/Download</a>
				</div>
				@endif

			</div>
			@if(!isset($showOnly))
			<div class="form-group row mt-5">
				<div class="col-sm-12 text-center">
					<button type="submit" class="btn btn-primary m-b-0">Update Employee</button>
				</div>
			</div>
			@endif
			{{ Form::close()}}
		</div>
	</div>
</div>
@endSection
