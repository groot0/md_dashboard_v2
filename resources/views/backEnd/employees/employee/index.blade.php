@extends('backEnd.master')
@section('mainContent')

@if(session()->has('message-success'))
<div class="alert alert-success mb-3 background-success" role="alert">
	{{ session()->get('message-success') }}
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
</div>
@elseif(session()->has('message-danger'))
<div class="alert alert-danger">
	{{ session()->get('message-danger') }}
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
</div>
@endif
@if(session()->has('message-success-delete'))
<div class="alert alert-danger mb-3 background-danger" role="alert">
	{{ session()->get('message-success-delete') }}
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
</div>
@elseif(session()->has('message-danger-delete'))
<div class="alert alert-danger">
	{{ session()->get('message-danger-delete') }}
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
</div>
@endif

<div class="container-fluid container_margin">
	<div class="card">
		<div class="card-header">
			<h5>Employee Lists</h5>
			<a href="{{ route('employee.create') }}" style="float: right; padding: 8px;" class="btn btn-success"> Add Employee </a>
		</div>
		<div class="card-block">
			<div class="dt-responsive table-responsive">
				<table id="basic-btn" class="table table-striped table-bordered nowrap basic-btn">
					<thead>
						<tr>
							<th>ID</th>
							<th>Photo</th>
							<th>Full Name</th>
							<th>Mobile</th>
							<th>Email</th>
							<th>Employee type</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						@php $i = 1 @endphp
						@foreach($employees as $employee)
						<tr>
							<td>{{$i++}}</td>
							<td>
								<div class="d-inline-block align-middle">
									@if(empty($employee->employee_photo))
										<img src="{{asset('public/images/no_image.png')}}" alt="user image" class="img-radius img-40 align-top m-r-15">
									@else
										<img src="{{asset($employee->employee_photo)}}" alt="user image" class="img-radius img-40 align-top m-r-15">
									@endif
								</div>
							</td>
							<td>{{$employee->full_name}}</td>
							<td>{{$employee->mobile}}</td>
							<td>{{$employee->email}}</td>
							<td>
							@if(isset($employee->employee_type_id))
								@if($employee->employee_type_id == 13)
								<button type="button" class="btn btn-secondary btn-sm">{{$employee->employee_type->base_setup_name}}</button>
								@endif

								@if($employee->employee_type_id == 14)
								<button type="button" class="btn btn-info btn-sm">{{$employee->employee_type->base_setup_name}}</button>
								@endif

								@if($employee->employee_type_id == 15)
							   <button type="button" class="btn btn-success btn-sm">{{$employee->employee_type->base_setup_name}}</button>
								@endif

								@if($employee->employee_type_id == 16)
								<button type="button" class="btn btn-primary btn-sm">{{$employee->employee_type->base_setup_name}}</button>
								@endif

								@if($employee->employee_type_id == 17)
								<button type="button" class="btn btn-warning btn-sm">{{$employee->employee_type->base_setup_name}}</button>
								@endif

								@endif

							</td>

							<td>
								<!-- <a href="" title="view"><button type="button" class="btn btn-success action-icon"><i class="fa fa-eye"></i></button></a> -->
								<a href="{{ route('employee.show',$employee->id) }}" class="view_icon" title="View"><i class="fa fa-eye"></i></a>
								<a href="{{ route('employee.edit',$employee->id) }}" class="edit_icon" title="edit"><i class="fa fa-edit"></i></a>
								<a class="adjustment_icon" title="Adjust time" data-modal-size="modal-md" href="{{url('showAdjustEmployeeView', $employee->device_USERID)}}">
									<i class="fa fa-cog"></i>
								</a>
								<a class="modalLink delete_icon" title="Delete" data-modal-size="modal-md" href="{{url('deleteEmployeeView', $employee->id)}}">
									<i class="fa fa-trash"></i>
								</a>
								
							</td>

						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

@endSection
