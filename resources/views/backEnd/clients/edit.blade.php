@extends('backEnd.master')
@section('mainContent')
<div class="card">
	<div class="card-header">
		<h5>Edit Client</h5>
	</div>
	<div class="card-block">
		<!-- <form id="main" method="post" action="http://html.phoenixcoded.net/" novalidate> -->
		<form method="post" action="{{ route('client.update', $client->id) }}">
			@method('PATCH')
	        @csrf
	        <div class="row">

	        	<div class="form-group col-md-6">
	            	<label for="client_name">Client Name:</label>
	            	<input type="text" class="form-control {{ $errors->has('client_name') ? ' is-invalid' : '' }}" name="client_name" value="{{ $client->client_name }}" />
	            	@if ($errors->has('client_name'))
					    <span class="invalid-feedback" role="alert">
							<span class="messages"><strong>{{ $errors->first('client_name') }}</strong></span>
						</span>
					@endif
	          	</div>
	        	
	        	<!-- <div class="form-group">
		              <label for="account_status">Account status:</label>
		              <input type="number" class="form-control" name="account_status" required="required" value="{{ $client->account_status }}"/>
		          </div> -->

		        <div class="form-group col-md-6">
		            <label for="client_contact">Client contact:</label>
		            <input type="text" class="form-control" name="client_contact" value="{{ $client->client_contact }}"/>
		        </div>

	        </div>

				  

		          <!-- <div class="form-group">
		              <label for="client_cur_bal">Client current balance:</label>
		              <input type="number" class="form-control" name="client_cur_bal" value="{{ $client->client_cur_bal }}"/>
		          </div> -->

		    <div class="row">

		        <div class="form-group col-md-6">
		            <label for="client_phone_1">Client Phone 1:</label>
		            <input type="text" class="form-control {{ $errors->has('client_phone_1') ? ' is-invalid' : '' }}" name="client_phone_1" value="{{ $client->client_phone_1 }}"/>
		            @if ($errors->has('client_phone_1'))
					    <span class="invalid-feedback" role="alert">
							<span class="messages"><strong>{{ $errors->first('client_phone_1') }}</strong></span>
						</span>
					@endif
		        </div>

		        <div class="form-group col-md-6">
		            <label for="client_phone_2">Client Phone 2:</label>
		            <input type="text" class="form-control" name="client_phone_2" value="{{ $client->client_phone_2 }}"/>
		        </div>
		
		    </div>

		    <div class="row">

		        <div class="form-group col-md-6">
		            <label for="client_fax">Client Fax:</label>
		            <input type="text" class="form-control" name="client_fax" value="{{ $client->client_fax }}"/>
		        </div>
				  
				<div class="form-group col-md-6">
		            <label for="email">Email:</label>
		            <input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $client->email }}"/>
		             @if ($errors->has('email'))
					    <span class="invalid-feedback" role="alert">
							<span class="messages"><strong>{{ $errors->first('email') }}</strong></span>
						</span>
					@endif
		        </div>

		    </div>

		          <!-- <div class="form-group">
		              <label for="client_account">Client Account:</label>
		              <input type="text" class="form-control" name="client_account" value="{{ $client->client_account }}"/>
		          </div>

		          <div class="form-group">
		              <label for="client_balance">Client Balance:</label>
		              <input type="number" class="form-control" name="client_balance" value="{{ $client->client_balance }}"/>
		          </div>

		          <div class="form-group">
		              <label for="client_paid">Client Paid:</label>
		              <input type="number" class="form-control" name="client_paid" value="{{ $client->client_paid }}"/>
		          </div>

				  <div class="form-group">
		              <label for="client_turnover">Client Turnover:</label>
		              <input type="number" class="form-control" name="client_turnover" value="{{ $client->client_turnover }}"/>
		          </div>

				  <div class="form-group">
		              <label for="last_invoice_number">Last Invoice Number:</label>
		              <input type="text" class="form-control" name="last_invoice_number" value="{{ $client->last_invoice_number }}"/>
		          </div>

		          <div class="form-group">
		              <label for="credit_limit">Credit Limit:</label>
		              <input type="number" class="form-control" name="credit_limit" value="{{ $client->credit_limit }}"/>
		          </div>

		          <div class="form-group">
		              <label for="amount_recevable">Amount Recevable:</label>
		              <input type="number" class="form-control" name="amount_recevable" value="{{ $client->amount_recevable }}"/>
		          </div>

		          <div class="form-group">
		              <label for="last_payment_date">Last Payment Date:</label>
		              <input type="date" class="form-control" name="last_payment_date" value="{{ $client->last_payment_date }}"/>
		          </div> -->
		    <div class="row"> 

			    <div class="form-group col-md-6">
			        <label for="client_remarks">Client Remarks:</label>
			        <textarea class="form-control" name="client_remarks">{{ $client->client_remarks }}</textarea>

			    </div>

			    <div class="form-group col-md-6">
			        
			    </div>
			</div>
		          <!-- <div class="form-group">
		              <label for="company_id">Company ID:</label>
		              <input type="text" class="form-control" name="company_id" value="{{ $client->company_id }}"/>
		          </div> -->
	        <div class="form-group row mt-5">
				<div class="col-sm-12 text-center">
					<button type="submit" class="btn btn-primary m-b-0">Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>

@endSection