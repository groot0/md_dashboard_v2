$("#search_attendance_btn").click(function() {

  let baseurl = $('meta[name="baseurl"]').attr("content"),
      fromdate = $("#date-picker-from").val(),
      todate = $("#date-picker-to").val(),
      employeeId = $("#select_employee").find(":selected").val();

  if(fromdate && todate && employeeId) {

    $.ajax({

      url: baseurl+'/search_employee_attendance',
      type: 'get',
      data: {
        fromdate: fromdate,
        todate: todate,
        employeeId: employeeId
      },
      success: function(response) {
        console.log("range response: ", response);

        $("#attendanceTable > tbody tr").remove();
        let attendances = response.attendances;
        attendances.forEach(attendance => {
          $('#attendanceTable').append(`<tr><td>${attendance.attendance_date}</td><td>${attendance.in_time}</td><td>${attendance.out_time}</td><td>${attendance.daily_hours}</td><td>${attendance.cumulative_hours}</td></tr>`);

        });
        $('#attendanceTable').append(`<tr><td colspan = "4"><b>Total hours: </b></td><td><b>${response.totalRangeHour}</b></td></tr>`);

      }
    });
  }
});


$("#export_pdf").click(function() {

  let baseurl = $('meta[name="baseurl"]').attr("content"),
      fromdate = $("#date-picker-from").val(),
      todate = $("#date-picker-to").val(),
      employeeId = $("#select_employee").find(":selected").val();

  window.open(
  `${baseurl}/search_employee_attendance?pdf=true&fromdate=${fromdate}&todate=${todate}&employeeId=${employeeId}`,
  '_blank'
);

});
