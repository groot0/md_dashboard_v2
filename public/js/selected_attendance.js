$(document).ready(function() {
  let baseurl = $('meta[name="baseurl"]').attr("content");
  $.ajax({

    url: baseurl+'/get_selected_attendance',
    type: 'get',
    success: function(response) {
      console.log("response: ", response);
      let employeeCount = 0,
          cellString = ``;

      response.forEach((employee, index, response) => {
        classes = "col-md-3 selected_attendance_cell";
        employeeCount++;

        if(employee.in_time !== 'n/a' && employee.out_time === 'n/a') classes += ' present';

        else classes += ' absent';

        cellString += `<div class="`+classes+`"><p>${employee.full_name}</p><p>${employee.in_time}</p><p>${employee.out_time}</p></div>`;

        if(employeeCount == 4) {

          $("#selectedAttendanceTable").append(`<div class="row">${cellString}</div>`);
          cellString = ``;
          employeeCount = 0;

        } else if(index === response.length - 1) {

          $("#selectedAttendanceTable").append(`<div class="row">${cellString}</div>`);

        }

      });
    },
    error: function(err) {

    }

  });

});
