
$(function(){

    $(".datepicker").datepicker({
        dateFormat: "dd-mm-yy",
        yearRange: '1950:2080',
        changeMonth: true,
        changeYear: true
    });
    // $("#datepicker").change(function() {
    //     var date = $(this).datepicker("getDate");
    //     if (date !== null) { // if any date selected in datepicker
    //         date instanceof Date; // -> true

    //         var make_date = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();
    //         // var url = '/md_dashboard/show_res_by_date/'+make_date;
    //         // window.location.href=url;

    //         $.ajax({
    //             url: '/show_res_by_date/sdfdsf',
    //         });

    //     }
    // });

    $("#divDatePicker").hide();

    $(".date_pick").click(function(){
      console.log("Here");

       $("#divDatePicker").toggle();
    });

    $("#divDatePicker").datepicker({
      onSelect: function(value, date) {
         //chose date
         $("#divDatePicker").hide();
         var make_date = date.currentYear+'-'+(date.currentMonth+1)+'-'+date.currentDay;
         var url = $('#url').val();
         url = url+'?date='+make_date;

         window.location.href=url;

      }
    });

    $("#date-picker-from, #date-picker-to" ).datepicker();

    $('.js-example-basic-single').select2();

    $("#btn_yesteday").click(function() {
      let url = $('meta[name=baseurl]').attr("content")+'?date=yesterday';
      console.log("url:", url);
      window.location.href=url;

    });

    $("#btn_today").click(function() {

      let url = $('meta[name=baseurl]').attr("content")+'?date=today';
      window.location.href=url;

    });

});



$('.input-effect input').each(function() {
	if ($(this).val().length > 0) {
		$(this).addClass('read-only-input');
	} else {
		$(this).removeClass('read-only-input');
	}

	$(this).on('keyup', function() {
		if ($(this).val().length > 0) {
			$(this).siblings('.invalid-feedback').fadeOut('slow');
		} else {
			$(this).siblings('.invalid-feedback').fadeIn('slow');
			$(this).removeClass('is-invalid');
		}
	});
});


// $(document).ready(function(){
// 	// **Side bar changes
// 	  	// get browser url,split and get last / url
// 		var get_url = $(location).attr("href").split('/').pop();


// 		//If there is home route which is / or "" then it will not process
// 		if(get_url != "") {
// 			// Get parent class of that last url, split, and get main class
// 			var get_parent_classes = $('.'+get_url).parent().parent().attr('class');
// 			//check for create,edit,update there is no parent class
// 			if(get_parent_classes != undefined) {
// 				var get_parent_classes = get_parent_classes.split(" ");
// 				var get_parent_class = '';
// 				for(var i = 0;i < get_parent_classes.length;i++) {
// 					if( get_parent_classes[i] != 'pcoded-hasmenu' || get_parent_classes[i] != 'pcoded-trigger' || get_parent_classes[i] != 'active' ) {
// 						get_parent_class = get_parent_classes[i];
// 					}
// 				}
// 				// this lines are for active those classes
// 				$('.pcoded-hasmenu').removeClass("active");
// 				$('.pcoded-hasmenu').removeClass("pcoded-trigger");
// 			    $('.'+get_parent_class).addClass("active");
// 			    $('.'+get_parent_class).addClass("pcoded-trigger");
// 			    $('.'+get_url).addClass("active");
// 			}
// 		}
// });


// This lines for toggle credit and debit amount in add-new-coa module
$('input[name=debit_credit_amount]').change(function(){
	var value = $( 'input[name=debit_credit_amount]:checked' ).val();
	if( value == 'debit' ) {
		$('.debit_div').show();
		$('.credit_div').hide();
	} else if( value == 'credit' ) {
		$('.debit_div').hide();
		$('.credit_div').show();
	}
});

$(document).ready(function() {
    $('body').on("click", ".modalLink", function(e) {
        e.preventDefault();
        $('.modal-backdrop').show();
        $("#showDetaildModal").show();
        $("div.modal-dialog").removeClass('modal-md');
        $("div.modal-dialog").removeClass('modal-lg');
        $("div.modal-dialog").removeClass('modal-bg');
        var modal_size = $(this).attr('data-modal-size');
        if (modal_size !== '' && typeof modal_size !== typeof undefined && modal_size !== false) {
            $("#modalSize").addClass(modal_size);
        } else {
            $("#modalSize").addClass('modal-md');
        }
        var title = $(this).attr('title');
        $("#showDetaildModalTile").text(title);
        var data_title = $(this).attr('data-original-title');
        $("#showDetaildModalTile").text(data_title);
        $("#showDetaildModal").modal('show');
        $('div.ajaxLoader').show();
        $.ajax({
            type: "GET",
            url: $(this).attr('href'),
            success: function(data) {
                // console.log('returned data: ', data);
                $("#showDetaildModalBody").html(data);
                $("#showDetaildModal").modal('show');
            }
        });
    });

    if ($.fn.DataTable.isDataTable("#employee_attendance_datatable")) {

      $('#employee_attendance_datatable').DataTable().destroy();

    }

    $( "#select_department" ).change(function() {
        if ($.fn.DataTable.isDataTable("#employee_attendance_datatable")) {
          $('#employee_attendance_datatable').DataTable().destroy();
        }

        var table = $('#employee_attendance_datatable').DataTable();
        var value = $('#select_department').val();
        table
            .column( 1 )
            .search( value )
            .draw();
    });

    // This section is for coloring yesterday and today btn start
    var url = $(location).attr('href'),
    parts = url.split("/"),
    last_part = parts[parts.length-1];
    if (last_part == 'yesterday') {
        $(".yesterday_btn").css("background-color", "#337ab7");
        $(".yesterday_btn").css("color", "white");
    } else if(last_part == 'today') {
        $(".today_btn").css("background-color", "#337ab7");
        $(".today_btn").css("color", "white");
    }
    // This section is for coloring yesterday and today btn end

    var table = $('#employee_attendance_datatable').DataTable({
      "order": []
    });

});


// Check if employee is selected or new employee
$('#employee_id').on('change', function() {
    var res = this.value;
    res = JSON.parse(res);
    var first_name = res.first_name;
    var last_name = res.last_name;
    var email = res.email;

    $('#first_name').val(first_name);
    $('#last_name').val(last_name);
    $('#email').val(email);

});
