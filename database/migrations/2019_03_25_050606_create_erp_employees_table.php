<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateErpEmployeesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('erp_employees', function (Blueprint $table) {
			$table->increments('id');
			$table->string('first_name')->nullable();
			$table->string('last_name')->nullable();
			$table->string('ud_employee_id')->nullable();
			$table->integer('device_Badgenumber')->nullable()->comment('previously named device_id');
			$table->integer('device_USERID')->nullable();
			$table->string('full_name')->nullable();
			$table->string('mobile')->nullable();
			$table->string('emergency_no')->nullable();
			$table->string('emergency_contact_name')->nullable();
			$table->string('emergency_contact_relation')->nullable();
			$table->string('email')->nullable();
			$table->date('date_of_birth')->nullable();
			$table->text('permanent_address')->nullable();
			$table->text('current_address')->nullable();
			$table->integer('department_id')->nullable();
			$table->integer('designation_id')->nullable();
			$table->integer('employee_type_id')->nullable();
			$table->date('joining_date')->nullable();
			$table->text('employee_photo')->nullable();
			$table->integer('gender_id')->nullable();
			$table->integer('blood_group_id')->nullable();
			$table->text('qualifications')->nullable();
			$table->text('experiences')->nullable();
			$table->text('job_description')->nullable();
			$table->text('employee_cv')->nullable();
			$table->text('employee_appointment_letter')->nullable();
			$table->tinyInteger('active_status')->default(1);
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
	}

	public function down() {
		Schema::dropIfExists('erp_employees');
	}
}
