<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateErpOfficeHoursTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('erp_office_hours', function (Blueprint $table) {
			$table->increments('id');
			$table->string('start_time')->nullable();
			$table->string('end_time')->nullable();
			$table->string('late_time')->nullable();
			$table->string('absent')->nullable();
			$table->tinyInteger('active_status')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('erp_office_hours');
	}
}
