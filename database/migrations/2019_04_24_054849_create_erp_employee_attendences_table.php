<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateErpEmployeeAttendencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erp_employee_attendences', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('device_USERID')->nullable();
            $table->string('in_time');
            $table->string('out_time');
            $table->string('check_type');
            $table->integer('adjusted')->default('0')->comment('0 = not changed, 1 = changed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('erp_employee_attendences');
    }
}