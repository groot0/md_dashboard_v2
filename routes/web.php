<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth'], function(){
	Route::get('/', 'HomeController@task_center');
	//Route::get('/home', 'HomeController@task_center');
	//Route::get('/dashboard', 'HomeController@task_center');


Route::get('mission_and_vision', 'HomeController@mission_and_vision');
Route::get('goals_and_objective', 'HomeController@goals_and_objective');
Route::get('task_center/{date}', 'HomeController@task_center');
Route::get('deliverables_and_deadlines', 'HomeController@deliverables_and_deadlines');
Route::get('performance_and_evaluations', 'HomeController@performance_and_evaluations');
Route::get('community_and_social_responsibility', 'HomeController@community_and_social_responsibility');

// Employee routes
Route::resource('employee', 'ErpEmployeeController');
Route::get('create_operation_modal', 'ErpEmployeeController@create_operation_modal');
Route::get('edit_operation_modal/{id}', 'ErpEmployeeController@edit_operation_modal');
Route::get('create_md_employee_modal', 'ErpEmployeeController@create_md_employee_modal');
Route::get('edit_md_employee_modal/{id}', 'ErpEmployeeController@edit_md_employee_modal');
Route::get('deleteEmployeeView/{id}', 'ErpEmployeeController@deleteEmployeeView');
Route::get('deleteEmployee/{id}', 'ErpEmployeeController@deleteEmployee');
Route::get('view_employee/{id}', 'ErpEmployeeController@viewEmployee');
Route::get('viewEmployeeById/{id}', 'ErpEmployeeController@viewEmployeeById');
Route::get('showAdjustEmployeeView/{device_USERID}', 'ErpEmployeeController@showAdjustEmployeeView');
Route::put('employee_time_adjust/{device_USERID}', 'ErpEmployeeController@employee_time_adjust');


//employee attendence
Route::resource('employee-attendence', 'ErpEmployeeAttendenceController');
Route::get('view-emplyoee-atten-details/{id}', 'ErpEmployeeAttendenceController@viewAttenDetails');
Route::post('attendence', 'ErpEmployeeAttendenceController@save_attendence');
Route::get('get-api-last-call-time', 'ErpEmployeeAttendenceController@getApiLastCallTime');
Route::post('emp_late_time', 'ErpEmployeeAttendenceController@emp_late_time');
//Route::get('emp_late_time', 'ErpEmployeeAttendenceController@emp_late_time');


// Base group routes
Route::resource('base_group', 'ErpBaseGroupController');
Route::get('deleteBaseGroupView/{id}', 'ErpBaseGroupController@deleteBaseGroupView');
Route::get('deleteBaseGroup/{id}', 'ErpBaseGroupController@deleteBaseGroup');

// Base setup routes
Route::resource('base_setup', 'ErpBaseSetupController');
Route::get('deleteBaseSetupView/{id}', 'ErpBaseSetupController@deleteBaseSetupView');
Route::get('deleteBaseSetup/{id}', 'ErpBaseSetupController@deleteBaseSetup');

// Designation routes
Route::resource('designation', 'ErpDesignationController');
Route::get('deleteDesignationView/{id}', 'ErpDesignationController@deleteDesignationView');
Route::get('deleteDesignation/{id}', 'ErpDesignationController@deleteDesignation');

// Sector routes
Route::resource('sector','SectorController');
Route::get('create_sector_modal', 'SectorController@create_sector_modal');
Route::get('edit_sector_modal/{id}', 'SectorController@edit_sector_modal');
Route::get('deleteSectorView/{id}', 'SectorController@deleteSectorView');
Route::get('deleteSector/{id}', 'SectorController@destroy');

// Company routes
Route::resource('company','CompanyController');
Route::get('create_company_modal', 'CompanyController@create_company_modal');
Route::get('edit_company_modal/{id}', 'CompanyController@edit_company_modal');
Route::get('deleteCompanyView/{id}', 'CompanyController@deleteSectorView');
Route::get('deleteCompany/{id}', 'CompanyController@destroy');

// Focus routes
Route::resource('focus','MdFocusController');
Route::get('edit_focus_modal/{id}', 'MdFocusController@edit_focus_modal');
Route::get('create_focus_modal', 'MdFocusController@create_focus_modal');
Route::get('deleteFocusView/{id}', 'MdFocusController@deleteFocusView');
Route::get('deleteFocus/{id}', 'MdFocusController@deleteFocus');

// Assign projects routes
Route::resource('assign_project','MdAssignProjectController');
Route::get('deleteAssignProjectView/{id}', 'MdAssignProjectController@deleteAssignProjectView');
Route::get('deleteAssignProject/{id}', 'MdAssignProjectController@deleteAssignProject');

// Project
Route::resource('project', 'ErpProjectController');
Route::get('create_project_modal', 'ErpProjectController@create_project_modal');
Route::get('edit_project_modal/{id}', 'ErpProjectController@edit_project_modal');
Route::get('deleteProjectView/{id}', 'ErpProjectController@deleteProjectView');
Route::get('deleteProject/{id}', 'ErpProjectController@deleteProject');

// Department routes
Route::resource('department', 'ErpDepartmentController');
Route::get('create_department_modal', 'ErpDepartmentController@create_department_modal');
Route::get('edit_department_modal/{id}', 'ErpDepartmentController@edit_department_modal');
Route::get('deleteDepartmentView/{id}', 'ErpDepartmentController@deleteDepartmentView');
Route::get('deleteDepartment/{id}', 'ErpDepartmentController@deleteDepartment');

// Assign task
Route::resource('emp_task_assign', 'MdAssignTaskController');
Route::get('assign_task/{id}', 'MdAssignTaskController@add_employee_task');
Route::get('view_assign_task/{id}', 'MdAssignTaskController@show_employee_task');

//For Generating report
Route::get('generate_report', 'HomeController@generate_report');
Route::post('search_fields', 'HomeController@generate_search_report');
// Route::get('calculate_hours', 'HomeController@calculate_hours');

Route::get('show_res_by_date/{date}', 'HomeController@show_res_by_date');

// Role route
Route::resource('role', 'ErpRoleController');
Route::get('deleteRoleView/{id}', 'ErpRoleController@deleteRoleView');
Route::get('deleteRole/{id}', 'ErpRoleController@deleteRole');


// User route
Route::resource('user', 'ErpUserController');
Route::get('deleteUserView/{id}', 'ErpUserController@deleteUserView');
Route::get('deleteUser/{id}', 'ErpUserController@deleteUser');

Route::get('fill_out_time_data', 'ErpEmployeeAttendenceController@fill_out_time_data');

Route::get('search_employee_attendance','ErpEmployeeAttendenceController@searchAttendance');

Route::get('/selected_attendance', 'ErpEmployeeAttendenceController@selected_attendance');
Route::get('/get_selected_attendance', 'ErpEmployeeAttendenceController@get_selected_attendance');

});
