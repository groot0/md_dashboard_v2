<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ErpDepartment extends Model
{
  public function employees() {
    return $this->hasMany('App\ErpEmployee','department_id','id');

 }
}
