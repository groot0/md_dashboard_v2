<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

use App\ErpEmployee;

class ErpEmployeeAttendence extends Model
{
    public function getAttendanceStatisticsByDate($target) {
      if($target == 'yesterday') {

        $targetDate = Carbon::now()->subDays(1);

      } else if($target == 'today' || $target == null) {

        $targetDate = Carbon::now();

      } else {

        $targetDate = Carbon::createFromFormat('Y-m-d', $target);

      }

      $total_employee = ErpEmployee::where('active_status',1)->count();

      $total_present = self::whereDate('in_time',$targetDate)->count();

      $total_absent = $total_employee - $total_present;

      return array('total'=>$total_employee, 'present'=>$total_present, 'absent' => $total_absent);

    }
}
