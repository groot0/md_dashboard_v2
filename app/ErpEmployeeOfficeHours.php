<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ErpEmployeeOfficeHours extends Model
{
    protected $fillable = [
	   'late_time',
	   'too_late',
	   'emp_id',
	];
}
