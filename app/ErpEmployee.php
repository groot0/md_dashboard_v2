<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class ErpEmployee extends Model {

  protected $guarded = [];

  protected static $targetDate;

  protected $in_time;

  protected $out_time;

  protected $today_total_hour;

  //Here we are setting the date instance

  public function __construct($target = null, array $attributes = array()) {

    if(self::$targetDate == null || $target != null) {

      if($target == 'yesterday') {

        self::$targetDate = Carbon::now()->subDays(1);

      } else if($target == 'today' || $target == null) {

        self::$targetDate = Carbon::now();

      } else {

        self::$targetDate = Carbon::createFromFormat('Y-m-d', $target);

      }

    }

    parent::__construct($attributes);

  }

 public function getEmployees() {

  return self::where('active_status',1)->get();

  }

    public function department(){
		return $this->belongsTo('App\ErpDepartment', 'department_id', 'id');
	}

	public function designation(){
		return $this->belongsTo('App\ErpDesignation', 'designation_id', 'id');
	}

	public function employee_type(){
		return $this->belongsTo('App\ErpBaseSetup', 'employee_type_id', 'id');
	}

  public function specialEmployee() {
    return $this->hasOne('App\ErpEmployeeOfficeHours', 'emp_id', 'id');
  }

  public function attendances() {
    return $this->hasMany('App\ErpEmployeeAttendence','device_USERID','device_USERID');

  }

  public function getInTime() {

    if($this->in_time) return $this->in_time;

    else {

      $this->initiateTodaysData();

      return $this->in_time;

    }

  }

  public function getOutTime() {

    if($this->out_time) return $this->out_time;

    else {

      $this->initiateTodaysData();

      $this->out_time;

    }

  }

//From here we are getting today's total hour

  public function today_total_hours() {

    if($this->today_total_hour) return $this->today_total_hour;

    else {

      $this->initiateTodaysData();

      return $this->today_total_hour;

    }

  }



  protected function initiateTodaysData() {
    if($this->today_total_hour) return true;
    $todays_attendance = $this->attendances()->whereDate('in_time',self::$targetDate)->first();
    $totalMinutes = 0;

    if($todays_attendance) {

      $to = Carbon::createFromFormat('Y-m-d H:i:s', $todays_attendance['in_time']);
      if($todays_attendance['out_time']) {

        $out = Carbon::createFromFormat('Y-m-d H:i:s', $todays_attendance['out_time']);
        $totalMinutes = $to->diffInMinutes($out);
        $this->out_time = $out->format('h:i a');
      } else if(Carbon::now()->diffInDays($to) == 0) {

        $this->out_time = 'n/a';
        $totalMinutes = $to->diffInMinutes(Carbon::now());
      }

      $this->today_total_hour = $this->minutesToHour($totalMinutes);
      $this->in_time = $to->format('h:i a');


    } else {

      $this->in_time = 'n/a';
      $this->out_time = 'n/a';
      $this->today_total_hour = 0;

    }



  }



// From Here we are getting the monthly hours

  public function monthly_total_hour() {
    $totalMinutes = 0;

    $attendances = $this->attendances()->whereMonth('in_time',self::$targetDate->month)->get();

    if($attendances->count() > 0) {

    foreach($attendances as $attendance) {

      $to = Carbon::createFromFormat('Y-m-d H:i:s', $attendance['in_time']);

      if($attendance['out_time']) {

          $totalMinutes+= $to->diffInMinutes(Carbon::createFromFormat('Y-m-d H:i:s', $attendance['out_time']));

      } else if(Carbon::now()->diffInDays($to) == 0) {

        $totalMinutes+= $to->diffInMinutes(Carbon::now());

      } else {

        $totalMinutes+= 0;

      }


    }

    return $this->minutesToHour($totalMinutes);

  }

    return $totalMinutes;

  }

  public function getAttendanceRange($from, $to) {
    $attendanceSearchResult = [];
    $totalMinutesInRange = 0;

    $attendances = $this->attendances()->whereBetween('in_time',[$from, $to])->get();

    foreach ($attendances as $attendance) {
      $in_time = Carbon::createFromFormat('Y-m-d H:i:s', $attendance['in_time']);

      $attendance['in_time'] = $in_time->format('h:i a');

      $attendance['attendance_date'] = $in_time->format('Y-m-d');

      if($attendance['out_time']) {
        $out_time = Carbon::createFromFormat('Y-m-d H:i:s', $attendance['out_time']);

        $totalMinutes = $in_time->diffInMinutes($out_time);

        $totalMinutesInRange += $totalMinutes;

        $attendance['daily_hours'] = $this->minutesToHour($totalMinutes);

        $attendance['out_time'] = $out_time->format('h:i a');

      } else {
        $attendance['daily_hours'] = 'n\a';

        $attendance['out_time'] = 'n\a';

      }
      $attendance['cumulative_hours'] = $this->minutesToHour($totalMinutesInRange);

      $attendanceSearchResult[] = $attendance;

    }

    return array("attendances" => $attendanceSearchResult, "totalRangeHour" => $this->minutesToHour($totalMinutesInRange));

  }

  protected function minutesToHour(int $minutes) {
    $hours = floor($minutes/60);

    $minutes = $minutes%60;

    return  $hours.' hrs '.$minutes.' mins';


  }

  public function sortingEmployeesByIntime($employees) {

    $emp_with_attendance = [];
    $emp_without_attendance = [];

    foreach($employees as $employee) {

        if($employee->getInTime() !== 'n/a') {

          $emp_with_attendance[] = $employee;

        } else {

          $emp_without_attendance[] = $employee;

        }

      }

      $attendance_count = sizeof($emp_with_attendance);

      for($c = 0; $c < $attendance_count - 1; $c++) {

        for($d = 0; $d < $attendance_count - $c - 1; $d++) {

          if(Carbon::createFromFormat('h:i a', $emp_with_attendance[$d]->getInTime())->greaterThan(Carbon::createFromFormat('h:i a', $emp_with_attendance[$d+1]->getInTime()))) {

            $swap = $emp_with_attendance[$d];
            $emp_with_attendance[$d] = $emp_with_attendance[$d+1];
            $emp_with_attendance[$d+1] = $swap;

          }

        }

      }

    $sorted_employees = array_merge($emp_with_attendance, $emp_without_attendance);

    return $sorted_employees;


  }

  public function rowColor($lateTime, $absentTime) {

    $inTime = $this->getInTime();

    // check in time exists
    if ($inTime != 'n/a') {

      $inTime = date("H:i:s", strtotime($inTime));

      //Check special employee start
      if( $this->specialEmployee['emp_id'] ) {

        $lateTime = $this->specialEmployee['late_time'];
        $absentTime = $this->specialEmployee['too_late'];

      }
      //Check special employee end

      if($inTime <= $lateTime) {

        return 'green_row';

      } else if($inTime >= $lateTime && $inTime <= $absentTime) {

        return 'yellow_row';

      } else {

        return 'red_row';

      }
    } else {

      return 'red_row';

    }

  }

  public function badgeColor( $rowColor ) {

    if ( $rowColor == 'green_row' ) {

      return '<span class="badge bg-c-green"></span>';

    } else if( $rowColor == 'yellow_row' ) {

      return '<span class="badge bg-c-green badge_yellow"></span>';

    } else {

      return '<span class="badge bg-c-green badge_red"></span>';

    }

  }

  public function checkLateTime( $empLateTime ) {


    //Check special employee start
    if( $this->specialEmployee['emp_id'] ) {

      return $this->specialEmployee['late_time'];

    }
    //Check special employee end

    return date('h:i', strtotime($empLateTime));

  }

  public function checkAbsentTime( $empAbsentTime ) {


    //Check special employee start
    if( $this->specialEmployee['emp_id'] ) {

      return $this->specialEmployee['too_late'];

    }
    //Check special employee end

    return date('h:i', strtotime($empAbsentTime));

  }

}
