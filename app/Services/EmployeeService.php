<?php
  namespace App\Services;

  use App\ErpEmployee;

  use Storage;

  use Auth;

  use Illuminate\Http\Request;

  class EmployeeService {

    public static function createEmployee($request) {

      if($request->hasFile('employee_photo')) {

        $employee_photo = self::uploadEmployeeFile($request['employee_photo'], 'employee_img','photo');

      }

      if($request->hasFile('employee_cv')) {

        $employee_cv = self::uploadEmployeeFile($request['employee_cv'], 'employee_cv','document');

      }

      if($request->hasFile('employee_appointment_letter')) {

        $employee_appointment_letter = self::uploadEmployeeFile($request['employee_appointment_letter'], 'employee_appointment_letter','document');

      }

      $newEmployee = ErpEmployee::create([

        'ud_employee_id' => $request['user_defined_employee_id'],
        'first_name' => $request['first_name'],
        'last_name' => $request['last_name'],
        'full_name' => $request['first_name'].' '.$request['last_name'],
        'mobile' => $request['mobile'],
        'emergency_no' => $request['emergency_no'],
        'email' => $request['email'],
        'date_of_birth' => $request['date_of_birth'] ? date('Y-m-d', strtotime($request['date_of_birth'])) : null,
        'permanent_address' => $request['permanent_address'],
        'current_address' => $request['current_address'],
        'department_id' => $request['department_id'],
        'designation_id' => $request['designation_id'],
        'joining_date' => $request['joining_date'] ? date('Y-m-d', strtotime($request['joining_date'])) : null,
        'employee_type_id' => $request['employee_type_id'],
        'gender_id' => $request['gender_id'],
        'blood_group_id' => $request['blood_group_id'],
        'qualifications' => $request['qualifications'],
        'experiences' => $request['experiences'],
        'job_description' => $request['job_description'],
        'employee_photo' => $employee_photo ? $employee_photo : null,
        'employee_cv' => $employee_cv ? $employee_cv : null,
        'employee_appointment_letter' => $employee_appointment_letter ? $employee_appointment_letter : null,
        'created_by' => Auth::user()->id

      ]);

      return $newEmployee;

    }


    public static function uploadEmployeeFile($file, $destination, $type) {

      $validExtensions = array(
        "photo" => ["jpg","jpeg","png","gif","tiff","svg"],
        "document" => ["pdf","doc","docx"]);

      $file_size_in_kb = round(($file->getSize() / 1024 ), 2);

      $file_extension = $file->getClientOriginalExtension();

      if(in_array($file_extension, $validExtensions[$type]) && $file_size_in_kb <= 10000.00) {

            $filename = time().$file->getClientOriginalName();

            $filePath = public_path('uploads/'.$destination);

            $file->move($filePath, $filename);

            return '/public/uploads/'.$destination.'/'.$filename;


      } else {

        return null;

      }


    }

  }

 ?>
