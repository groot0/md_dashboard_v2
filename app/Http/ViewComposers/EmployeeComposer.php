<?php

  namespace App\Http\ViewComposers;

  use Illuminate\View\View;

  use App\ErpBaseSetup;

  use App\ErpDepartment;

  use App\ErpDesignation;

  use App\ErpEmployee;

  class EmployeeComposer {


    public function compose(View $view) {

      $designations = ErpDesignation::all();

  		$departments = ErpDepartment::all();

      $genders = ErpBaseSetup::where('base_group_id', 1)->get();

  		$blood_groups = ErpBaseSetup::where('base_group_id', 2)->get();

  		$employee_types = ErpBaseSetup::where('base_group_id', 3)->get();

      $view->with(['designations' => $designations, 'departments' => $departments, 'blood_groups' => $blood_groups, 'employee_types' => $employee_types, 'genders' => $genders]);

    }


  }


 ?>
