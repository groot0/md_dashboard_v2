<?php

namespace App\Http\Controllers;

use App\ErpEmployeeAttendence;
use DB;
use Illuminate\Http\Request;
use App\ErpAttendenceApiTrack;
use Session;
use App\ErpEmployeeOfficeHours;
use Carbon\Carbon;
use PDF;
use App\ErpEmployee;
use App\ErpDepartment;

class ErpEmployeeAttendenceController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	 public function index() {

 		return view('backEnd.employees.attendences.index', [
 			'employees' => ErpEmployee::all(),
 			'departments' => ErpDepartment::all()
 		]);
 	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}

	public function viewAttenDetails($id) {

		$employee_name = ErpEmployeeAttendence::where('device_USERID', '=', $id)->first();

		$results = DB::select(DB::raw("SELECT *
			FROM erp_employee_attendences
			WHERE DATE(in_time) = CURDATE() AND device_USERID = $id"));
		return view('backEnd.employees.attendences.show', compact('results', 'employee_name'));
	}

	public function save_attendence(Request $request) {

		// $test = array();
		if (!empty($request['users'])) {
			foreach ($request['users'] as $individual) {

				if ($individual['CHECKTYPE'] == 'I') {
					$in_time = $individual['CHECKTIME'];
				} else {
					$out_time = null;
				}

				if ($individual['CHECKTYPE'] == 'O') {
					$in_time = $individual['CHECKTIME'];
				} else {
					$out_time = null;
				}

				$attendence = new ErpEmployeeAttendence();
				$attendence->device_USERID = $individual['USERID'];
				$attendence->name = $individual['Name'];
				$attendence->in_time = $in_time;
				$attendence->out_time = $out_time;
				$attendence->check_type = $individual['CHECKTYPE'];
				$result = $attendence->save();

				if ($result) {
					$data = array(
						'data_inserting_time' =>  date('Y-m-d h:i:s'),
					);
					DB::table('erp_attendence_api_tracks')
					->where('active_status', 1)
					->update($data);
				}
			}
		}

	}

	public function getApiLastCallTime() {

		$results = DB::select(DB::raw("select data_inserting_time from erp_attendence_api_tracks where active_status =
			1"));
		if(!empty($results)){
			$last_current_date_time = $results[0]->data_inserting_time;
			// Session::put('data_inserting_time', $last_current_date_time);
		}

		return response()->json($last_current_date_time);
		//return view('backEnd.employees.employee.create', compact('last_current_date_time'));
	}

	public function searchAttendance(Request $request) {

		$employeeId = $request['employeeId'];
		$fromDate = $request['fromdate'];
		$toDate = $request['todate'];


		$formattedFromDate = Carbon::createFromFormat('m/d/Y', $fromDate);

		$formattedToDate = Carbon::createFromFormat('m/d/Y', $toDate);

		$employee = ErpEmployee::find($employeeId);

		$attendances = $employee->getAttendanceRange($formattedFromDate, $formattedToDate);

		// if we found the request for pdf this will execute
		if($request['pdf']) {
			$attendanceFrom = $formattedFromDate->format('Y-m-d');
			$attendanceTo = $formattedToDate->format('Y-m-d');

			$pdf = PDF::loadView('backEnd.pdfLayout.attendance_report', compact('attendances','attendanceFrom', 'attendanceTo', 'employee'));
			return $pdf->stream('attendance.pdf');
		}

		return response()->json($attendances);

}

	public function emp_late_time(){
		$late_time = $_POST['late_time'];
		$too_late = $_POST['too_late'];
		$emp_id = $_POST['emp_id'];

		ErpEmployeeOfficeHours::updateOrCreate(
		    [
		        'emp_id' => $emp_id
		    ],
		    [
		        'late_time' => $late_time,
		        'too_late' => $too_late
		    ]
		);
	}

	public function fill_out_time_data(){

		for($day = 1; $day<17; $day++){
			$results = DB::select(DB::raw("SELECT * FROM `erp_employee_attendences_replica`
				WHERE DATE(in_time) = '" . date('Y-m-d', strtotime('-'.$day.' days')) . "'  AND check_type = 'O'"));

			if(!empty($results)){
				foreach($results as $value){
					$result = DB::select(DB::raw("UPDATE `erp_employee_attendences_replica`
						SET out_time =  '$value->in_time'
						WHERE DATE(in_time) = '" . date('Y-m-d', strtotime('-'.$day.' days')) . "'  AND check_type = 'I' AND device_USERID = '$value->device_USERID'"));

					$resultss = DB::select(DB::raw("DELETE FROM `erp_employee_attendences_replica` WHERE DATE(in_time) = '" . date('Y-m-d', strtotime('-'.$day.' days')) . "'  AND check_type = 'O'"));

				}
			}
		}
	}

	public function selected_attendance() {

	return view('backEnd.employees.attendences.selected_employee_attendance');

}


public function get_selected_attendance() {
	$employees = ErpEmployee::all()->take(10);

	foreach ($employees as $employee) {

		$employee['in_time'] = $employee->getInTime();
		$employee['out_time'] = $employee->getOutTime();

	}

	return response()->json($employees);

}

}
