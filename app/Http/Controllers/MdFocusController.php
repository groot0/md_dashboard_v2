<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MdFocus;
use Auth;

class MdFocusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'focus_name'=>'required'
        ]);
            
        $focus = new MdFocus();
        $focus->focus_name = $request->get('focus_name');
        $focus->description = $request->get('description');
        if (isset($request->focus_end_date)) {
            $focus->focus_end_date = date('Y-m-d', strtotime($request->focus_end_date));
        } else {
            $focus->focus_end_date = $request->focus_end_date;
        }

        $focus->created_by = Auth::user()->id;

        $result = $focus->save();
        if($result) {
            return redirect()->back()->with('message-success', 'Focus has been added.');
        } else {
            return redirect()->back()->with('message-danger', 'Something went wrong.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $editData = MdFocus::find($id);
        // $focuses = MdFocus::where('active_status', '=', '1')->get();
        // return view('backEnd.projects.edit', compact('editData', 'focuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'focus_name'=>'required'
        ]);
            
        $focus = MdFocus::find($id);
        $focus->focus_name = $request->get('focus_name');
        $focus->description = $request->get('description');
        if (isset($request->focus_end_date)) {
            $focus->focus_end_date = date('Y-m-d', strtotime($request->focus_end_date));
        } else {
            $focus->focus_end_date = $request->focus_end_date;
        }
        $focus->updated_by = Auth::user()->id;

        $result = $focus->update();
        if($result) {
            return redirect()->back()->with('message-success', 'Focus has been updated.');
        } else {
            return redirect()->back()->with('message-danger', 'Something went wrong.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteFocusView($id){
        $module = 'deleteFocus';
         return view('backEnd.showDeleteModal', compact('id','module'));
    }

    public function deleteFocus($id){
        $sectorDestroy = MdFocus::where('id', $id)->update([
            'active_status' => 0
        ]);

        if($sectorDestroy){
            return redirect()->back()->with('message-success-delete', 'Focus has been deleted successfully');
        }else{
            return redirect()->back()->with('message-danger-delete', 'Something went wrong, please try again');
        }
    }

    // Customized function for focus edit modal
    public function edit_focus_modal($id) {
        $focuses = MdFocus::where('id','=',$id)->get();
        return view('backEnd.task_center.focus.edit', compact('focuses'));
    }

    // Customized function for focus edit modal
    public function create_focus_modal() {

        return view('backEnd.task_center.focus.create');

    }
}
