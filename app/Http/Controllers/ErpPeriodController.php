<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ErpPeriod;
use Auth;

class ErpPeriodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $periods = ErpPeriod::all();
        return view('backEnd.chart_of_accounts.period.index', compact('periods'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'period_name' => "required",
            'period_starts' => "required",
            'period_ends' => "required"
            
        ]);

       $period = new ErpPeriod();
       $period->period_name = $request->period_name;
       $period->period_starts = date('Y-m-d', strtotime($request->period_starts));
       $period->period_ends = date('Y-m-d', strtotime($request->period_ends));
       $period->period_closed = $request->period_closed;
       $period->created_by = Auth::user()->id;
       $results = $period->save();

       if($results){
           return redirect()->back()->with('message-success', 'Period has been added successfully');
       }else{
           return redirect()->back()->with('message-danger', 'Something went wrong, please try again');
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editData = ErpPeriod::find($id);
        $periods = ErpPeriod::all();
        return view('backEnd.chart_of_accounts.period.index', compact('editData', 'periods'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'period_name' => "required",
            'period_starts' => "required",
            'period_ends' => "required"
           
        ]);

       $period = ErpPeriod::find($id);
       $period->period_name = $request->period_name;
       $period->period_starts = date('Y-m-d', strtotime($request->period_starts));
       $period->period_ends = date('Y-m-d', strtotime($request->period_ends));
       $period->period_closed = $request->period_closed;
       $period->updated_by = Auth::user()->id;
       $results = $period->update();

       if($results){
           return redirect()->back()->with('message-success', 'Period has been updated successfully');
       }else{
           return redirect()->back()->with('message-danger', 'Something went wrong, please try again');
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deletePeriodView($id){
         return view('backEnd.chart_of_accounts.period.deletePeriodView', compact('id'));
    }

    public function deletePeriod($id){
        $result = ErpPeriod::destroy($id);
        if($result){
            return redirect()->back()->with('message-success-delete', 'Period has been deleted successfully');
        }else{
            return redirect()->back()->with('message-danger-delete', 'Something went wrong, please try again');
        }
    }
}
