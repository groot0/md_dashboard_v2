<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\MdAssignProject;
use App\ErpEmployee;
use App\ErpProject;

class MdAssignTaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'employee_id' => 'required',
            'project_id' => 'required'
        ]);

        $project = new MdAssignProject();
        $project->project_id = $request->get('project_id');
        $project->employee_id = $request->get('employee_id');
        $project->task_name = $request->get('task_name');
        $project->task_start_date = self::check_date_value( $request->task_start_date );
        $project->task_end_date = self::check_date_value( $request->task_end_date );
        $project->updated_by = Auth::user()->id;

        $result = $project->save();
        
        if($result) {
            return redirect()->back()->with('message-success', 'Project assign has been assigned.');
        } else {
            return redirect()->back()->with('message-danger', 'Something went wrong.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'employee_id' => 'required',
            'project_id' => 'required'
        ]);

        $check_assigned_project = MdAssignProject::where('employee_id', '=', $id)->get();

        if( count( $check_assigned_project) <= 0) {
            $project = new MdAssignProject();
            $project->project_id = $request->get('project_id');
            $project->employee_id = $request->get('employee_id');
            $project->task_name = $request->get('task_name');
            $project->task_start_date = self::check_date_value( $request->task_start_date );
            $project->task_end_date = self::check_date_value( $request->task_end_date );
            $project->updated_by = Auth::user()->id;

            $result = $project->save();
        } else {
            $project = MdAssignProject::where('employee_id', '=', $id)->first();
            $project->project_id = $request->get('project_id');
            $project->employee_id = $request->get('employee_id');
            $project->task_name = $request->get('task_name');
            $project->task_start_date = self::check_date_value( $request->task_start_date );
            $project->task_end_date = self::check_date_value( $request->task_end_date );
            $project->updated_by = Auth::user()->id;
            $result = $project->update();
        }
        
        if($result) {
            return redirect()->back()->with('message-success', 'Project assign has been assigned.');
        } else {
            return redirect()->back()->with('message-danger', 'Something went wrong.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // Customized functions
    public function add_employee_task($id) {
        $assign_project = MdAssignProject::where('employee_id','=',$id)->get();
        $employees = ErpEmployee::where('active_status', '=', 1)->where('employee_type_id', '=', 13)->get();
        $projects = ErpProject::where('active_status', '=', 1)->get();
        return view('backEnd.task_center.emp_assign_task.emp_assign_task', compact('id','assign_project','employees','projects'));
    }

    public function show_employee_task($id) {
        $assign_projects = MdAssignProject::where('employee_id','=',$id)->get();
        $employees = ErpEmployee::where('active_status', '=', 1)->where('employee_type_id', '=', 13)->get();
        $projects = ErpProject::where('active_status', '=', 1)->get();
        return view('backEnd.task_center.emp_assign_task.show_emp_assign_task', compact('id','assign_projects','employees','projects'));
    }

    // Customized function for checking date null or not
    public function check_date_value( $date_value ) {
        if (isset($date_value)) {
            return date('Y-m-d', strtotime($date_value));
        } else {
            return $date_value;
        }
    }

    
}
