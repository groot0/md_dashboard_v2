<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'name' => 'required'

        ]);
        
        $company = Company::create($request->all());

        if ($company) return redirect()->back()->with('message-success', 'Company has been added');

        else return redirect()->back()->with('message-danger', 'Something went wrong while adding company');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        $companyUpdate = $company->update($request->except(['_token','_method']));

        if($companyUpdate) {

            return redirect()->back()->with('message-success','Company updated successfully');

          } else {

            return redirect()->back()->with('message-danger', 'Something went wrong while updating company');

          }
    }

    public function deleteSectorView($id){

        $module = 'deleteCompany';

        return view('backEnd.showDeleteModal', compact('id','module'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $deleteCompany = Company::where('id', $id)->update([
            'active_status' => 0
        ]);

        if($deleteCompany) {
            return redirect()->back()->with('message-success-delete', 'Company has been deleted successfully');
        } else {
            return redirect()->back()->with('message-danger-delete', 'Something went wrong, please try again');
        }
        
    }

    // Customized function for focus edit modal
    public function create_company_modal() {
        return view('backEnd.company.create');
    }

    // Customized function for focus edit modal
    public function edit_company_modal($id) {
        $companies = Company::where('id','=',$id)->get();
        return view('backEnd.company.edit', compact('companies'));
    }

    
}
