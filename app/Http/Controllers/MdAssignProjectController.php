<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\MdAssignProject;

class MdAssignProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'project_id'=>'required',
            'employee_id' => 'required'
        ]);
            
        $project = new MdAssignProject();
        $project->project_id = $request->get('project_id');
        $project->employee_id = $request->get('employee_id');
        $project->task_name = $request->get('task_name');
        $project->task_start_date = self::check_date_value( $request->task_start_date );
        $project->task_end_date = self::check_date_value( $request->task_end_date );
        $project->created_by = Auth::user()->id;

        $result = $project->save();

        if($result) {
            return redirect()->back()->with('message-success', 'Project assign has been added.');
        } else {
            return redirect()->back()->with('message-danger', 'Something went wrong.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'project_id'=>'required',
            'employee_id' => 'required'
        ]);
            
        $project = MdAssignProject::find($id);
        $project->project_id = $request->get('project_id');
        $project->employee_id = $request->get('employee_id');
        $project->task_name = $request->get('task_name');
        $project->task_start_date = self::check_date_value( $request->task_start_date );
        $project->task_end_date = self::check_date_value( $request->task_end_date );
        $project->updated_by = Auth::user()->id;

        $result = $project->update();
        
        if($result) {
            return redirect()->back()->with('message-success', 'Project assign has been updated.');
        } else {
            return redirect()->back()->with('message-danger', 'Something went wrong.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteAssignProjectView($id){
        $module = 'deleteAssignProject';
         return view('backEnd.showDeleteModal', compact('id','module'));
    }

    public function deleteAssignProject($id){
        $projectDestroy = MdAssignProject::where('id', $id)->update([
            'active_status' => 0
        ]);

        if($projectDestroy){
            return redirect()->back()->with('message-success-delete', 'Project assign has been deleted successfully');
        }else{
            return redirect()->back()->with('message-danger-delete', 'Something went wrong, please try again');
        }
    }

    // Customized function for checking date null or not
    public function check_date_value( $date_value ) {
        if (isset($date_value)) {
            return date('Y-m-d', strtotime($date_value));
        } else {
            return $date_value;
        }
    }
}
