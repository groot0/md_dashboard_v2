<?php

namespace App\Http\Controllers;

use App\Sector;
use Illuminate\Http\Request;

class SectorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'sector_name'=>'required'

        ]);

        $sector = Sector::create([

            'name' => $request->sector_name

        ]);

        if($sector)  {

            return redirect()->back()->with('message-success', 'Sector has been added');

        } else {

            return redirect()->back()->with('message-danger', 'Something went wrong while adding sector');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sector  $sector
     * @return \Illuminate\Http\Response
     */
    public function show(Sector $sector)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sector  $sector
     * @return \Illuminate\Http\Response
     */
    public function edit(Sector $sector)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sector  $sector
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sector $sector)
    {

        $request->validate([

            'sector_name'=>'required'

        ]);

        $sectorUpdate = Sector::where('id', $sector->id)->update([

            'name' => $request->sector_name

        ]);


        if($sectorUpdate) {

            return redirect()->back()->with('message-success','Sector updated successfully');

          } else {

            return redirect()->back()->with('message-danger', 'Something went wrong while updating sector');

          }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sector  $sector
     * @return \Illuminate\Http\Response
     */

    public function deleteSectorView($id){

        $module = 'deleteSector';

        return view('backEnd.showDeleteModal', compact('id','module'));
    }

    public function destroy($id) {

        $sectorDestroy = Sector::where('id', $id)->update([

            'active_status' => 0

        ]);

        if($sectorDestroy) {

            return redirect()->back()->with('message-success-delete', 'Sector has been deleted successfully');

        } else {

            return redirect()->back()->with('message-danger-delete', 'Something went wrong, please try again');
        }

    }

    // Customized function for focus edit modal
    public function create_sector_modal() {

        return view('backEnd.sector.create');

    }

    // Customized function for focus edit modal
    public function edit_sector_modal($id) {
        $sectors = Sector::where('id','=',$id)->get();
        return view('backEnd.sector.edit', compact('sectors'));
    }

    
}
