<?php

namespace App\Http\Controllers;

use App\ErpBaseSetup;
use App\ErpDepartment;
use App\ErpDesignation;
use App\ErpEmployee;
use App\Services\EmployeeService;
use Auth;
use Illuminate\Http\Request;
use App\ErpEmployeeAttendence;
use App\ErpEmployeeOfficeHours;
use App\ErpOfficeHours;

class ErpEmployeeController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$employees = ErpEmployee::where('active_status', '=', 1)->get();
		return view('backEnd.employees.employee.index', compact('employees'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return view('backEnd.employees.employee.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$request->validate([
			'first_name' => 'required',
			'last_name' => 'required',
			'mobile' => 'required',
		]);

		$employee = EmployeeService::createEmployee($request);

		if($employee) {

				return redirect('/employee')->with('message-success', 'Employee has been added');

		} else {

				return redirect('/employee')->with('message-danger', 'Something went wrong');

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$showOnly = true;
		$editData = ErpEmployee::find($id);
		return view('backEnd.employees.employee.edit', compact('editData','showOnly'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$editData = ErpEmployee::find($id);
		return view('backEnd.employees.employee.edit', compact('editData'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$request->validate([
			'first_name' => 'required',
			'last_name' => 'required',
			'mobile' => 'required',
			'device_USERID' => 'required|unique:erp_employees,device_USERID,'.$id
		]);

		$employee = ErpEmployee::find($id);
		$employee->ud_employee_id = $request->get('user_defined_employee_id');
		$employee->device_USERID = $request->get('device_USERID');
		$employee->first_name = $request->get('first_name');
		$employee->last_name = $request->get('last_name');
		$full_name = $request->get('first_name') . ' ' . $request->get('last_name');
		$employee->full_name = $full_name;
		$employee->mobile = $request->get('mobile');
		$employee->emergency_no = $request->get('emergency_no');
		$employee->emergency_contact_name = $request->get('emergency_contact_name');
		$employee->emergency_contact_relation = $request->get('emergency_contact_relation');
		$employee->email = $request->get('email');
		if (isset($request->date_of_birth)) {
			$employee->date_of_birth = date('Y-m-d', strtotime($request->date_of_birth));
		} else {
			$employee->date_of_birth = $request->date_of_birth;
		}
		$employee->permanent_address = $request->get('permanent_address');
		$employee->current_address = $request->get('current_address');
		$employee->department_id = $request->get('department_id');
		$employee->designation_id = $request->get('designation_id');
		if (isset($request->joining_date)) {
			$employee->joining_date = date('Y-m-d', strtotime($request->joining_date));
		} else {
			$employee->joining_date = $request->joining_date;
		}
		$employee->employee_type_id = $request->get('employee_type_id');
		// $employee_type_name = $request->get('employee_type_id');

		// if($employee_type_name == 'mds_desk') {
		//     $employee->employee_type_id = 13;
		// } else if($employee_type_name == 'operations') {
		//     $employee->employee_type_id = 14;
		// } else if($employee_type_name == 'permanent') {
		//     $employee->employee_type_id = 15;
		// } else if($employee_type_name == 'temporary') {
		//     $employee->employee_type_id = 16;
		// } else {
		//     $employee->employee_type_id = $request->get('employee_type_id');
		// }

		//Employee image upload
		$invalid_extentension_flag = 0;
        if ($request->hasFile('employee_photo')) {
            $extensions = ["jpg","jpeg","png","gif","tiff","svg"];
            $image = $request->file('employee_photo');
            $image_name = $image->getClientOriginalName();
            $file_extension_name = $image->getClientOriginalExtension();
            $image_size = $request->file('employee_photo')->getSize();
            $image_size_in_kb = round( ( $image_size / 1024 ), 2);

            if(! in_array($file_extension_name, $extensions) || $image_size_in_kb > 10000.00 ){
              $invalid_extentension_flag = 1;
              return redirect('/employee/'.$id.'/edit')->with('message-danger-img', 'Employee Image File format or size does not');
            }
            $destinationPath = public_path('/uploads/employee_img');
            if ($invalid_extentension_flag == 0) {
            	$str=rand();
				$image_name = md5($str);
                $image_name = $image_name.'.'.$file_extension_name;
                $image->move($destinationPath, $image_name);

                // Delete previous file Start
	            if($employee->employee_photo != ''){
	            	$previous_employee_img = base_path().$employee->employee_photo;
	            	if (file_exists($previous_employee_img)) {
	                	unlink($previous_employee_img);
				    }
	            }
	            // Delete previous file End

                $employee->employee_photo = '/public/uploads/employee_img/'.$image_name;
            }
        }

		$employee->gender_id = $request->get('gender_id');
		$employee->blood_group_id = $request->get('blood_group_id');
		$employee->qualifications = $request->get('qualifications');
		$employee->experiences = $request->get('experiences');
		$employee->job_description = $request->get('job_description');

		// Employee cv upload
		$invalid_extentension_flag = 0;
		if ($request->hasFile('employee_cv')) {
            $extensions = ["pdf","doc","docx"];
            $file = $request->file('employee_cv');
            $file_name = $file->getClientOriginalName();
            $file_extension_name = $file->getClientOriginalExtension();
            $file_size = $request->file('employee_cv')->getSize();
            $file_size_in_kb = round( ( $file_size / 1024 ), 2);

            if(! in_array($file_extension_name, $extensions) || $file_size_in_kb > 10000.00 ){
              $invalid_extentension_flag = 1;
              return redirect('/employee/'.$id.'/edit')->with('message-danger-cv', 'Employee Resume File format or size does not');
            }
            $destinationPath = public_path('/uploads/employee_cv');
            if ($invalid_extentension_flag == 0) {
            	$file_name = md5($file_name).'.'.$file_extension_name;
                $file->move($destinationPath, $file_name);

                // Delete previous file Start
	            if($employee->employee_cv != ''){
	            	$previous_employee_cv = base_path().$employee->employee_cv;
	            	if (file_exists($previous_employee_cv)) {
	                	unlink($previous_employee_cv);
				    }
	            }
	            // Delete previous file End

                $employee->employee_cv = '/public/uploads/employee_cv/'.$file_name;
            }
        }

        // Employee appointment letter upload
		$invalid_extentension_flag = 0;
		if ($request->hasFile('employee_appointment_letter')) {
            $extensions = ["pdf","doc","docx"];
            $file = $request->file('employee_appointment_letter');
            $file_name = $file->getClientOriginalName();
            $file_extension_name = $file->getClientOriginalExtension();
            $file_size = $request->file('employee_appointment_letter')->getSize();
            $file_size_in_kb = round( ( $file_size / 1024 ), 2);

            if(! in_array($file_extension_name, $extensions) || $file_size_in_kb > 10000.00 ){
              $invalid_extentension_flag = 1;
              return redirect('/employee/'.$id.'/edit')->with('message-danger-appo', 'Employee Appointment File format or size does not');
            }
            $destinationPath = public_path('/uploads/employee_appointment_letter');
            if ($invalid_extentension_flag == 0) {
            	$file_name = md5($file_name).'.'.$file_extension_name;
                $file->move($destinationPath, $file_name);

                // Delete previous file Start
	            if($employee->employee_appointment_letter != ''){
	            	$previous_employee_appointment_letter = base_path().$employee->employee_appointment_letter;
	            	if (file_exists($previous_employee_appointment_letter)) {
	                	unlink($previous_employee_appointment_letter);
				    }
	            }
	            // Delete previous file End

                $employee->employee_appointment_letter = '/public/uploads/employee_appointment_letter/'.$file_name;
            }
        }

		$employee->updated_by = Auth::user()->id;

		$results = $employee->update();
		if ($results) {
			return redirect('/employee')->with('message-success', 'Employee has been updated');
		} else {
			return redirect('/employee')->with('message-danger', 'Something went wrong');
		}

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function deleteEmployeeView($id) {
		$module = 'deleteEmployee';
		return view('backEnd.showDeleteModal', compact('id', 'module'));
	}

	public function deleteEmployee($id) {
		$employee = ErpEmployee::find($id);
		$employee->active_status = 0;

		$results = $employee->update();
		if ($results) {
			return redirect('/task_center')->with('message-success-delete', 'Employee has been deleted successfully');
		} else {
			return redirect('/task_center')->with('message-danger-delete', 'Something went wrong, please try again');
		}
	}

	// Customized function for create operation employee
	public function create_operation_modal() {
		$designations = ErpDesignation::all();
		return view('backEnd.task_center.operation_employee.create', compact('designations'));
	}

	// Customized function for edit operation employee
	public function edit_operation_modal($id) {
		$operation_employees = ErpEmployee::where('id', '=', $id)->get();
		$designations = ErpDesignation::all();
		return view('backEnd.task_center.operation_employee.edit', compact('operation_employees', 'designations'));
	}

	// Customized function for create operation employee
	public function create_md_employee_modal() {
		$designations = ErpDesignation::all();
		return view('backEnd.task_center.employee.create', compact('designations'));
	}

	// Customized function for edit operation employee
	public function edit_md_employee_modal($id) {
		$mds_employees = ErpEmployee::where('id', '=', $id)->get();
		$designations = ErpDesignation::all();
		return view('backEnd.task_center.employee.edit', compact('mds_employees', 'designations'));
	}

	public function viewEmployee($id) {
		
		$employee = ErpEmployee::whereId($id)->first();
		$employee_special_case = ErpEmployeeOfficeHours::whereEmp_id($employee->id)->first();
		$all_employee_late_time_details = ErpOfficeHours::whereActive_status(1)->first();

		return view('backEnd.task_center.employee.view_employee_details', compact('employee', 'employee_special_case', 'all_employee_late_time_details'));
	}

    public function viewEmployeeById($id) {
        $employee = ErpEmployee::where('id', '=', $id)->first();
        return view('backEnd.task_center.employee.view_employee_details', compact('employee'));
    }

    //Get this employee in time and out time
    public function showAdjustEmployeeView($device_USERID) {
    	$employee = ErpEmployee::where('device_USERID', '=', $device_USERID)->first();
    	$get_emp_device_user_id = $employee->device_USERID;
    	$employee_attendences = ErpEmployeeAttendence::where('device_USERID','=',$get_emp_device_user_id)->where('adjusted','=',1)->get();
    	return view('backEnd.employees.employee.adjust_time',compact('employee', 'employee_attendences'));
    }

    //Adjustment time and date by employee id
    public function employee_time_adjust(Request $request, $device_USERID) {
    	$date = $request->date;
    	$in_time = $request->in_time;
    	$out_time = $request->out_time;
    	$emp_full_name = $request->emp_full_name;

    	// Check date
    	if ($date) {
    		$date = Date('Y-m-d', strtotime($date));
    		$start_date = $date.' 00:00:01';
    		$end_date = $date.' 23:59:59';
    		// Check in time or out time exist or not
    		if ($in_time || $out_time) {
    			$error_flag = 0;

    			//For In time save or update
    			if ($in_time) {
    				$get_this_emp_atten = ErpEmployeeAttendence::where('device_USERID', '=', $device_USERID)->where('check_type', '=', 'I')->whereBetween('in_time', [$start_date, $end_date])->first();
    				//Update in if and Save in else
    				if ($get_this_emp_atten) {
    					$get_this_emp_atten->in_time = $date.' '.$in_time.':00';
    					$get_this_emp_atten->adjusted = 1;
    					$update_atten = $get_this_emp_atten->update();
    					if (!$update_atten) {
    						$error_flag = 1;
    					}
    				} else {
    					//Save new attendence to erp_employee_attendence table
    					$this_emp_atten = new ErpEmployeeAttendence();
    					$this_emp_atten->device_USERID = $device_USERID;
    					$this_emp_atten->name = $emp_full_name;
    					$this_emp_atten->in_time = $date.' '.$in_time.':00';
    					$this_emp_atten->check_type = 'I';
    					$this_emp_atten->adjusted = 1;
    					$save_atten = $this_emp_atten->save();
    					if (!$save_atten) {
    						$error_flag = 1;
    					}
    				}
    			}

    			// For out time save or update
    			if($out_time) {
    				$get_this_emp_atten = ErpEmployeeAttendence::where('device_USERID', '=', $device_USERID)->where('check_type', '=', 'O')->whereBetween('in_time', [$start_date, $end_date])->orderBy('in_time', 'desc')->first();
    				//Update in if and Save in else
    				if ($get_this_emp_atten) {
    					$get_this_emp_atten->in_time = $date.' '.$out_time.':00';
    					$get_this_emp_atten->adjusted = 1;
    					$update_atten = $get_this_emp_atten->update();
    					if (!$update_atten) {
    						$error_flag = 1;
    					}
    				} else {
    					//Save new attendence to erp_employee_attendence table
    					$this_emp_atten = new ErpEmployeeAttendence();
    					$this_emp_atten->device_USERID = $device_USERID;
    					$this_emp_atten->name = $emp_full_name;
    					$this_emp_atten->in_time = $date.' '.$out_time.':00';
    					$this_emp_atten->check_type = 'O';
    					$this_emp_atten->adjusted = 1;
    					$save_atten = $this_emp_atten->save();
    					if (!$save_atten) {
    						$error_flag = 1;
    					}
    				}
    			}

    			if ($error_flag == 0) {
    				return redirect('showAdjustEmployeeView/'.$device_USERID)->with('message-success', 'Saved successfully.');
    			} else {
    				return redirect('showAdjustEmployeeView/'.$device_USERID)->with('message-danger', 'Something went wrong. Please Try again later.');
    			}
    		} else {
    			return redirect('showAdjustEmployeeView/'.$device_USERID)->with('message-danger', 'Please select In Time or Out Time.');
    		}
    	} else {
    		return redirect('showAdjustEmployeeView/'.$device_USERID)->with('message-danger', 'Please select a date.');
    	}
    }
}
