<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ErpClient;
use Auth;

class ErpClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = ErpClient::all();
        return view('backEnd.clients.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backEnd.clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'client_name'=>'required',
            'client_phone_1' => 'required',
            'email' => 'required'
            // 'account_status'=> 'required|integer',
            // 'company_id' => 'required'
        ]);

        $client = new ErpClient();
        $client->client_name = $request->get('client_name');
        // $client->account_status = $request->get('account_status');
        $client->client_contact = $request->get('client_contact');
        // $client->client_cur_bal = $request->get('client_cur_bal');
        $client->client_phone_1 = $request->get('client_phone_1');
        $client->client_phone_2 = $request->get('client_phone_2');
        $client->client_fax = $request->get('client_fax');
        $client->email = $request->get('email');
        // $client->client_account = $request->get('client_account');
        // $client->client_balance = $request->get('client_balance');
        // $client->client_paid = $request->get('client_paid');
        // $client->client_turnover = $request->get('client_turnover');
        // $client->last_invoice_number = $request->get('last_invoice_number');
        // $client->credit_limit = $request->get('credit_limit');
        // $client->amount_recevable = $request->get('amount_recevable');
        // $client->last_payment_date = $request->get('last_payment_date');
        $client->client_remarks = $request->get('client_remarks');
        if ($request->hasFile('client_image')) {
            $image = $request->file('client_image');
            $image_name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/client_img');
            $imagePath = $destinationPath. "/".  $image_name;
            $image->move($destinationPath, $image_name);
            $client->client_image = '/public/uploads/client_img/'.$image_name;
        }
        $client->created_by = Auth::user()->id;
        // $client->company_id = $request->get('company_id');

        $client->save();
        return redirect('/client')->with('message-success', 'Client has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = ErpClient::find($id);
        return view('backEnd.clients.show', compact('client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = ErpClient::find($id);
        return view('backEnd.clients.edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'client_name'=>'required',
            'client_phone_1' => 'required',
            'email' => 'required'
            // 'account_status'=> 'required|integer',
            // 'company_id' => 'required'
        ]);

        $client = ErpClient::find($id);
        $client->client_name = $request->get('client_name');
        // $client->account_status = $request->get('account_status');
        $client->client_contact = $request->get('client_contact');
        // $client->client_cur_bal = $request->get('client_cur_bal');
        $client->client_phone_1 = $request->get('client_phone_1');
        $client->client_phone_2 = $request->get('client_phone_2');
        $client->client_fax = $request->get('client_fax');
        $client->email = $request->get('email');
        // $client->client_account = $request->get('client_account');
        // $client->client_balance = $request->get('client_balance');
        // $client->client_paid = $request->get('client_paid');
        // $client->client_turnover = $request->get('client_turnover');
        // $client->last_invoice_number = $request->get('last_invoice_number');
        // $client->credit_limit = $request->get('credit_limit');
        // $client->amount_recevable = $request->get('amount_recevable');
        // $client->last_payment_date = $request->get('last_payment_date');
        $client->client_remarks = $request->get('client_remarks');
        if ($request->hasFile('client_image')) {
            $image = $request->file('client_image');
            $image_name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/client_img');
            $imagePath = $destinationPath. "/".  $image_name;
            $image->move($destinationPath, $image_name);
            $client->client_image = '/public/uploads/client_img/'.$image_name;
        }
        $client->updated_by = Auth::user()->id;
        // $client->company_id = $request->get('company_id');

        $client->update();
        return redirect('/client')->with('message-success', 'Client has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     $client = ErpClient::find($id);
    //     $client->delete();

    //     return redirect('/client')->with('message-success', 'Client has been deleted Successfully');
    // }

     public function deleteClientView($id){
         return view('backEnd.clients.deleteClientView', compact('id'));
    }

    public function deleteClient($id){
        $result = ErpClient::destroy($id);
        if($result){
            return redirect()->back()->with('message-success-delete', 'Clients  has been deleted successfully');
        }else{
            return redirect()->back()->with('message-danger-delete', 'Something went wrong, please try again');
        }
    }
}
