<?php

namespace App\Http\Controllers;

use App\Company;
use App\ErpDepartment;
use App\ErpDesignation;
use App\ErpEmployee;
use App\ErpEmployeeAttendence;
use App\ErpProject;
use App\MdAssignProject;
use App\MdFocus;
use App\Sector;
use DB;
use Illuminate\Http\Request;
use App\ErpOfficeHours;
use App\ErpEmployeeOfficeHours;

class HomeController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function index() {
		return url('backEnd.dashboard');
	}

	public function mission_and_vision() {
		$companies = Company::where('active_status', 1)->get();
		return view('backEnd.mission_and_vision', compact('companies'));
	}

	public function goals_and_objective() {
		return view('backEnd.goals_and_objective');
	}

	public function task_center(Request $request) {
		$targetDate = $request['date'];
		if($targetDate == null) {
			$targetDate = date('Y-m-d');
		}

		$employeeModel = new ErpEmployee($targetDate);
		$all_employees = $employeeModel->getEmployees();

		$sortedEmployees = $employeeModel->sortingEmployeesByIntime($all_employees);

		//dd($sortedEmployees);

		$employee_attendance_statistics = (new ErpEmployeeAttendence())->getAttendanceStatisticsByDate($targetDate);

		$employees = ErpEmployee::where('active_status', '=', 1)->where('employee_type_id', '=', 13)->get();

		$departments = ErpDepartment::where('active_status', '=', 1)->get();

		$assign_projects = MdAssignProject::where('active_status', '=', 1)->get();

		$office_times = ErpOfficeHours::where('active_status', '=', 1)->first();
		// This lines for getting current month data end

		// Get special employee times
		$special_emp_office_hours = ErpEmployeeOfficeHours::all();
		// Get special employee times

		return view('backEnd.task_center', compact('office_times','employees', 'departments', 'special_emp_office_hours','employee_attendance_statistics','sortedEmployees'));
	}

	public function deliverables_and_deadlines() {
		return view('backEnd.deliverables_and_deadlines');
	}

	public function performance_and_evaluations() {
		return view('backEnd.performance_and_evaluations');
	}

	public function community_and_social_responsibility() {
		return view('backEnd.community_and_social_responsibility');
	}

	public function generate_report() {
		$departments = ErpDepartment::where('active_status', '=', 1)->get();
		$employees = ErpEmployee::where('active_status', '=', 1)->get();
		return view('backEnd.individual_files.generate_report', compact('departments', 'employees'));
	}

	public function generate_search_report(Request $request) {
		$departments = ErpDepartment::where('active_status', '=', 1)->get();
		$employees = ErpEmployee::where('active_status', '=', 1)->get();
		$department_id = $request->department_id;
		if ($department_id == '') {
			$department = '';
		} else {
			$department = ' AND ed.id = ' . $department_id;
		}
		$device_USERID = $request->device_USERID;
		if ($device_USERID == '') {
			$employee = '';
		} else {
			$employee = ' AND eea.device_USERID = ' . $device_USERID;
		}
		if (isset($request->from_date)) {
			$from_date = date('Y-m-d', strtotime($request->from_date));
			$temp_from_date = $from_date;
			$from_date = "'" . $from_date . " 00:00:01'";
		} else {
			$from_date = "'" . date('Y-m-d') . " 00:00:01'";
			$temp_from_date = date('Y-m-d');
		}

		if (isset($request->to_date)) {
			$to_date = date('Y-m-d', strtotime($request->to_date));
			$to_date = "'" . $to_date . " 23:23:59'";
		} else {
			$to_date = "'" . $temp_from_date . " 23:23:59'";
		}

		$sql = "SELECT eea.device_USERID AS device_USERID, eea.name AS employee_name, eea.in_time, eea.check_type, ed.department_name, ed.id AS department_id
            FROM erp_employee_attendences AS eea
            INNER JOIN erp_employees AS ee ON ee.id=eea.device_USERID
            INNER JOIN erp_departments AS ed ON ed.id = ee.department_id
            WHERE (eea.in_time BETWEEN $from_date AND $to_date)" . $department . "" . $employee . "";

		$results = DB::select($sql);
		return view('backEnd.individual_files.generate_report', compact('results', 'departments', 'employees', 'sql'));
	}

	// public function calculate_hours() {
	//     $first_day_this_month = date('Y-m-01');
	//     $last_day_this_month  = date('Y-m-t');

	//     $this_month_employee_attendences_data = ErpEmployeeAttendence::whereBetween('in_time', [$first_day_this_month, $last_day_this_month])->get();
	//     foreach ($this_month_employee_attendences_data as $values) {
	//         if($values->check_type == 'I') {
	//             $emp[$values->device_USERID]['in_time'] = $values->in_time;
	//         }
	//     }
	// }

	public function show_res_by_date($date) {

		$all_employees = ErpEmployee::where('active_status', '=', 1)->get();
		$all_employees_count = $all_employees->count();
		$employees = ErpEmployee::where('active_status', '=', 1)->where('employee_type_id', '=', 13)->get();
		$operational_employees = ErpEmployee::where('active_status', '=', 1)->where('employee_type_id', '=', 14)->get();
		$designations = ErpDesignation::where('active_status', '=', 1)->get();
		$sectors = Sector::where('active_status', 1)->get();
		$companies = Company::where('active_status', 1)->get();
		$departments = ErpDepartment::where('active_status', '=', 1)->get();
		$focuses = MdFocus::where('active_status', '=', 1)->get();
		$all_employees = ErpEmployee::where('active_status', '=', 1)->get();
		$projects = ErpProject::where('active_status', '=', 1)->get();
		$assign_projects = MdAssignProject::where('active_status', '=', 1)->get();
		// $result = DB::select( DB::raw("SELECT *
		//             FROM erp_employee_attendences
		//             WHERE DATE(in_time) = CURDATE()"));

		if ($date == 'today') {
			$result = DB::select(DB::raw("SELECT * FROM erp_employee_attendences WHERE DATE(in_time) = '" . date('Y-m-d') . "'"));
			$present_office = DB::select( DB::raw("SELECT DISTINCT device_USERID
		            FROM erp_employee_attendences
		            WHERE DATE(in_time) = CURDATE() AND check_type = 'I' "));



		} else if ($date == 'yesterday') {
			$result = DB::select(DB::raw("SELECT * FROM erp_employee_attendences WHERE DATE(in_time) = '" . date('Y-m-d', strtotime('-1 days')) . "'"));
			$present_office = DB::select( DB::raw("SELECT DISTINCT device_USERID
		            FROM erp_employee_attendences
		            WHERE DATE(in_time) = '" . date('Y-m-d', strtotime('-1 days')) . "' AND check_type = 'I' "));


		} else {
			$result = DB::select(DB::raw("SELECT * FROM erp_employee_attendences WHERE DATE(in_time) = '" . $date . "'"));
			$present_office = DB::select( DB::raw("SELECT DISTINCT device_USERID
		            FROM erp_employee_attendences
		            WHERE DATE(in_time) = '" . $date . "' AND check_type = 'I' "));
		}

		//get employee id, user device id, full name and department id start
		$dept_id_sql = "SELECT emp.id, emp.device_USERID, emp.full_name, emp.department_id, dept.department_name FROM `erp_employees` AS emp, erp_departments AS dept WHERE emp.department_id = dept.id";
		$department_with_employees = DB::select(DB::raw($dept_id_sql));
		//get employee id, user device id, full name and department id start

		// $present_in_office = DB::select( DB::raw("SELECT DISTINCT device_USERID
		//             FROM erp_employee_attendences
		//             WHERE DATE(in_time) = CURDATE() AND check_type = 'I' "));
		// $present_in_office_count = count( $present_in_office );

		// This lines for getting current month data start

		//https://stackoverflow.com/questions/2680501/how-can-i-find-the-first-and-last-date-in-a-month-using-php
		$first_day_this_month = date('Y-m-01');
		$last_day_this_month = date('Y-m-t');

		$this_month_employee_attendences_data = ErpEmployeeAttendence::whereBetween('in_time', [$first_day_this_month, $last_day_this_month])->get();
		$office_times = ErpOfficeHours::where('active_status', '=', 1)->first();
		// This lines for getting current month data end

		return view('backEnd.task_center', compact('office_times','employees', 'operational_employees', 'designations', 'sectors', 'companies', 'departments', 'focuses', 'all_employees', 'projects', 'assign_projects', 'result', 'this_month_employee_attendences_data', 'department_with_employees','date', 'present_office', 'all_employees_count'));
	}
}
