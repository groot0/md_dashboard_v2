<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ErpRole;
use App\ErpModule;
use App\Erp_role_permissions;
use Auth;
use App\Http\Requests\RoleRequest;

class ErpRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = ErpRole::whereActive_status(1)->get();
        return view('backEnd.roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {

        ErpRole::create([
            'role_name' => request('role_name'),
            'created_by' => Auth::user()->id
        ]);       

        return redirect('/role')->with('message-success', 'Role has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editData = ErpRole::findOrFail($id);
        $roles = ErpRole::whereActive_status(1)->get();

        return view('backEnd.roles.index', compact('editData', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request, $id)
    {

        ErpRole::findOrFail($id)->update([
            'role_name' => request('role_name'),
            'updated_by' => Auth::user()->id
        ]);

        return redirect('/role')->with('message-success', 'Role has been updated');    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     //
    // }
    public function deleteRoleView($id){
        $module = 'deleteRole';
         return view('backEnd.showDeleteModal', compact('id','module'));
    }

    public function deleteRole($id){
        
        ErpRole::findOrFail($id)->update([
            'active_status' => 0,
            'updated_by' => Auth::user()->id
        ]);

        return redirect()->back()->with('message-success-delete', 'Role has been deleted successfully');
    }

    public function assignPermission($role_id){
        $role = ErpRole::find($role_id);
        $modules = ErpModule::where('active_status', 1)->get();
        $role_permissions = Erp_role_permissions::where('role_id', $role_id)->get();
        $already_assigned = [];
        foreach($role_permissions as $role_permission){
            $already_assigned[] = $role_permission->module_link_id;
        }
        return view('backEnd.roles.assignPermission', compact('role', 'modules', 'already_assigned'));
    }

    public function rolePermissionStore(Request $request){
        Erp_role_permissions::where('role_id', $request->role_id)->delete();

        if(isset($request->permissions)){
            foreach($request->permissions as $permission){
                $role_permission = new Erp_role_permissions();
                $role_permission->role_id = $request->role_id;
                $role_permission->module_link_id = $permission;
                $role_permission->save();
            }
        }

        return redirect('role')->with('message-success-assign-role', 'Role permission has been assigned successfully');
    }
}
