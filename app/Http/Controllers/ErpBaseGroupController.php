<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ErpBaseGroup;
use Auth;

class ErpBaseGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $base_groups = ErpBaseGroup::where('active_status','=',1)->get();
        return view('backEnd.base_group.index', compact('base_groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required'
        ]);
            
        $base_group = new ErpBaseGroup();
        $base_group->name = $request->get('name');
        $base_group->created_by = Auth::user()->id;

        $results = $base_group->save();
        if($results) {
            return redirect('/task_center')->with('message-success', 'Group has been added');
        } else {
            return redirect('/task_center')->with('message-danger', 'Something went wrong');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editData = ErpBaseGroup::find($id);
        $base_groups = ErpBaseGroup::where('active_status','=',1)->get();
        return view('backEnd.base_group.index', compact('editData', 'base_groups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
           'name' => "required"
        ]);
       
       $base_group = ErpBaseGroup::find($id);
       $base_group->name = $request->name;
       $base_group->updated_by = Auth::user()->id;
       $results = $base_group->update();

       if($results){
           return redirect('task_center')->with('message-success', 'Group has been updated successfully');
       }else{
           return redirect('task_center')->with('message-danger', 'Something went wrong, please try again');
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteBaseGroupView($id){
        $module = 'deleteBaseGroup';
         return view('backEnd.showDeleteModal', compact('id','module'));
    }

    public function deleteBaseGroup($id){
        $base_group = ErpBaseGroup::find($id);
        $base_group->active_status = 0;

        $results = $base_group->update();

        if($results){
            return redirect('task_center')->with('message-success-delete', 'Group has been deleted successfully');
        }else{
            return redirect('task_center')->with('message-danger-delete', 'Something went wrong, please try again');
        }
    }
}
