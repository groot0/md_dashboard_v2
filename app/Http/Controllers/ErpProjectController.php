<?php

namespace App\Http\Controllers;

use App\ErpClient;
use App\ErpProject;
use Auth;
use Illuminate\Http\Request;

class ErpProjectController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$projects = ErpProject::all();
		return view('backEnd.projects.index', compact('projects'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$clients = ErpClient::where('active_status', '=', '1')->get();
		return view('backEnd.projects.create', compact('clients'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$request->validate([
			'project_name' => 'required',
		]);

		$project = new ErpProject();
		$project->project_name = $request->get('project_name');
		$project->project_start_date = self::check_date_value($request->project_start_date);
		// $project->project_status = $request->get('project_status');
		$project->project_end_date = self::check_date_value($request->project_end_date);
		$project->project_amount = $request->get('project_amount');
		$project->client_id = $request->get('client_id');
		$project->advances_received = $request->get('advances_received');
		$project->last_date_of_receipt = self::check_date_value($request->last_date_of_receipt);
		$project->completion_due_date = self::check_date_value($request->completion_due_date);
		$project->completed_on = self::check_date_value($request->completed_on);
		$project->receipts_to_date = $request->get('receipts_to_date');
		$project->expenses_to_date = $request->get('expenses_to_date');
		$project->created_by = Auth::user()->id;

		$project->save();
		return redirect('/task_center')->with('message-success', 'Project has been added');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$editData = ErpProject::find($id);
		$clients = ErpClient::where('active_status', '=', '1')->get();
		return view('backEnd.projects.show', compact('editData', 'clients'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$editData = ErpProject::find($id);
		$clients = ErpClient::where('active_status', '=', '1')->get();
		return view('backEnd.projects.edit', compact('editData', 'clients'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$request->validate([
			'project_name' => 'required',
		]);

		$project = ErpProject::find($id);
		$project->project_name = $request->get('project_name');
		$project->project_start_date = self::check_date_value($request->project_start_date);
		// $project->project_status = $request->get('project_status');
		$project->project_end_date = self::check_date_value($request->project_end_date);
		$project->project_amount = $request->get('project_amount');
		$project->client_id = $request->get('client_id');
		$project->advances_received = $request->get('advances_received');
		$project->last_date_of_receipt = self::check_date_value($request->last_date_of_receipt);
		$project->completion_due_date = self::check_date_value($request->completion_due_date);
		$project->completed_on = self::check_date_value($request->completed_on);
		$project->receipts_to_date = $request->get('receipts_to_date');
		$project->expenses_to_date = $request->get('expenses_to_date');
		$project->updated_by = Auth::user()->id;

		$project->save();
		return redirect('/task_center')->with('message-success', 'Project has been updated');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	// public function destroy($id)
	// {
	//     $project = ErpProject::find($id);
	//     $project->delete();

	//     return redirect('/project')->with('message-success', 'Project has been deleted Successfully');
	// }

	public function deleteProjectView($id) {
		return view('backEnd.projects.deleteProjectView', compact('id'));
	}

	public function deleteProject($id) {
		$result = ErpProject::destroy($id);
		if ($result) {
			return redirect()->back()->with('message-success-delete', 'Project has been deleted successfully');
		} else {
			return redirect()->back()->with('message-danger-delete', 'Something went wrong, please try again');
		}
	}

	// Customized function for checking date null or not
	public function check_date_value($date_value) {
		if (isset($date_value)) {
			return date('Y-m-d', strtotime($date_value));
		} else {
			return $date_value;
		}
	}

	// Customized function for department create modal
	public function create_project_modal() {
		return view('backEnd.task_center.project.create');
	}

	// Customized function for focus edit modal
	public function edit_project_modal($id) {
		$projects = ErpProject::where('id', '=', $id)->get();
		return view('backEnd.task_center.project.edit', compact('projects'));
	}

}
