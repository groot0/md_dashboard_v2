<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ErpUser extends Model
{
    protected $guarded = ['id', 'email_verified_at', 'image', 'remember_token' 'created_at', 'updated_at'];
}
